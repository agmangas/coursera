"""
Mini-max Tic-Tac-Toe Player
"""

import poc_ttt_gui
import poc_ttt_provided as provided

# Set timeout, as mini-max can take a long time

import codeskulptor
codeskulptor.set_timeout(60)

# SCORING VALUES - DO NOT MODIFY

SCORES = {
    provided.PLAYERX: 1,
    provided.DRAW: 0, 
    provided.PLAYERO: -1
}

def mm_move(board, player):
    """
    Make a move on the board.
    
    Returns a tuple with two elements.  The first element is the score
    of the given board and the second element is the desired move as a
    tuple, (row, col).
    """

    check_win = board.check_win()

    if check_win is not None:
        return SCORES[check_win], (-1, -1)

    scores, moves = list(), list()

    for emptysq in board.get_empty_squares():

        board_clone = board.clone()
        board_clone.move(emptysq[0], emptysq[1], player)
        score, dummy_move = mm_move(board_clone, provided.switch_player(player))

        if score * SCORES[player] == 1:
            return score, (emptysq[0], emptysq[1])

        scores.append(score)
        moves.append((emptysq[0], emptysq[1]))

    scores_mod = [score * SCORES[player] for score in scores]
    max_idx = scores_mod.index(max(scores_mod))

    return scores[max_idx], moves[max_idx]

def move_wrapper(board, player, trials):
    """
    Wrapper to allow the use of the same infrastructure that was used
    for Monte Carlo Tic-Tac-Toe.
    """

    move = mm_move(board, player)
    assert move[1] != (-1, -1), "returned illegal move (-1, -1)"
    return move[1]

# Test game with the console or the GUI.
# Uncomment whichever you prefer.
# Both should be commented out when you submit for
# testing to save time.

# provided.play_game(move_wrapper, 1, False)        
# poc_ttt_gui.run_gui(3, provided.PLAYERO, move_wrapper, 1, False)
