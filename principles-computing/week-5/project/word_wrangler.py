"""
Student code for Word Wrangler game
"""

import urllib2
import codeskulptor
import poc_wrangler_provided as provided

WORDFILE = "assets_scrabble_words3.txt"

# Functions to manipulate ordered word lists

def remove_duplicates(list1):
    """
    Eliminate duplicates in a sorted list.

    Returns a new sorted list with the same elements in list1, but
    with no duplicates.

    This function can be iterative.
    """

    ret = list()

    for val in list1:
        if not val in ret:
            ret.append(val)

    return ret

def _dict_from_list(lst):
    """
    Returns a dict where the keys are the unique elements 
    of the list and the values are the number of occurences 
    of each value.
    """

    ret = dict()

    for val in lst:
        ret[val] = ret.get(val, 0) + 1

    return ret

def _decrease_dict_key(dct, key):
    """
    Decreases the value of dct[key] and pops key if 
    the resulting value is zero.
    """

    dct[key] -= 1
    if dct[key] == 0:
        dct.pop(key)

def intersect(list1, list2):
    """
    Compute the intersection of two sorted lists.

    Returns a new sorted list containing only elements that are in
    both list1 and list2.

    This function can be iterative.
    """

    ret = list()

    list2d = _dict_from_list(list2)

    for val in list1:
        if val in list2d:
            ret.append(val)
            _decrease_dict_key(list2d, val)

    return ret

# Functions to perform merge sort

def merge(list1, list2):
    """
    Merge two sorted lists.

    Returns a new sorted list containing all of the elements that
    are in either list1 and list2.

    This function can be iterative.
    """

    ret = list()

    list1cp, list2cp = list1[:], list2[:]

    while len(list1cp) or len(list2cp):

        if not len(list1cp):
            ret.append(list2cp.pop(0))
            continue

        if not len(list2cp):
            ret.append(list1cp.pop(0))
            continue

        if list1cp[0] <= list2cp[0]:
            ret.append(list1cp.pop(0))
        else:
            ret.append(list2cp.pop(0))

    return ret
                
def merge_sort(list1):
    """
    Sort the elements of list1.

    Return a new sorted list with the same elements as list1.

    This function should be recursive.
    """

    if len(list1) <= 1:
        return list1
    
    sub = list1[:len(list1)/2], list1[len(list1)/2:]
    sorted_sub = merge_sort(sub[0]), merge_sort(sub[1])

    return merge(sorted_sub[0], sorted_sub[1])


# Function to generate all strings for the word wrangler game

def gen_all_strings(word):
    """
    Generate all strings that can be composed from the letters in word
    in any order.

    Returns a list of all strings that can be formed from the letters
    in word.

    This function should be recursive.
    """

    if not len(word):
        return [""]

    first, rest = word[0], word[1:]
    rest_strings = gen_all_strings(rest)
    new_strings = list()

    for reststr in rest_strings:
        for idx in xrange(len(reststr) + 1):
            new_string = reststr[:idx] + first + reststr[idx:]
            new_strings.append(new_string)

    return rest_strings + new_strings

# Function to load words from a file

def load_words(filename):
    """
    Load word list from the file named filename.

    Returns a list of strings.
    """

    ret = list()
    
    dicth = urllib2.urlopen(codeskulptor.file2url(filename))

    for line in dicth.readlines():
        ret.append(line.strip())

    return ret

def run():
    """
    Run game.
    """
    words = load_words(WORDFILE)
    wrangler = provided.WordWrangler(words, remove_duplicates, 
                                     intersect, merge_sort, 
                                     gen_all_strings)
    provided.run_game(wrangler)

# Uncomment when you are ready to try the game
# run()

    
    
