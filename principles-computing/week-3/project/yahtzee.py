"""
Planner for Yahtzee
Simplifications:  only allow discard and roll, only score against upper level
"""

# Used to increase the timeout, if necessary

import codeskulptor
codeskulptor.set_timeout(20)


def gen_all_sequences(outcomes, length):
    """
    Iterative function that enumerates the set of all sequences of
    outcomes of given length.
    """

    answer_set = set([()])
    for dummy_idx in range(length):
        temp_set = set()
        for partial_sequence in answer_set:
            for item in outcomes:
                new_sequence = list(partial_sequence)
                new_sequence.append(item)
                temp_set.add(tuple(new_sequence))
        answer_set = temp_set
    return answer_set


def score(hand):
    """
    Compute the maximal score for a Yahtzee hand according to the
    upper section of the Yahtzee score card.

    hand: full yahtzee hand

    Returns an integer score 
    """

    if not len(hand):
        return float(0)

    scores = dict()

    for value in hand:
        if not value in scores:
            scores[value] = 0
        scores[value] += value

    return max(scores.values())


def expected_value(held_dice, num_die_sides, num_free_dice):
    """
    Compute the expected value of the held_dice given that there
    are num_free_dice to be rolled, each with num_die_sides.

    held_dice: dice that you will hold
    num_die_sides: number of sides on each die
    num_free_dice: number of dice to be rolled

    Returns a floating point expected value
    """

    held_dice = tuple(held_dice)

    all_sequences = gen_all_sequences(
        [val + 1 for val in range(num_die_sides)],
        num_free_dice
    )

    all_scores = [score(seq + held_dice) for seq in all_sequences]

    return float(sum(all_scores)) / len(all_scores)


def gen_all_holds(hand):
    """
    Generate all possible choices of dice from hand to hold.

    hand: full yahtzee hand

    Returns a set of tuples, where each tuple is dice to hold
    """

    holds = set()

    for idx in xrange(2 ** len(hand)):

        bword = bin(idx)[2:]
        bword = "0" * (len(hand) - len(bword)) + bword

        new_hold = tuple()

        for bword_idx in xrange(len(bword)):

            if bword[bword_idx] == "1":
                new_hold = new_hold + (hand[bword_idx], )

        holds.add(new_hold)

    return holds


def strategy(hand, num_die_sides):
    """
    Compute the hold that maximizes the expected value when the
    discarded dice are rolled.

    hand: full yahtzee hand
    num_die_sides: number of sides on each die

    Returns a tuple where the first element is the expected score and
    the second element is a tuple of the dice to hold
    """

    holds = list(gen_all_holds(hand))

    expected_values = list()

    for hold in holds:
        expected_values.append(
            expected_value(hold, num_die_sides, len(hand) - len(hold))
        )

    max_score = max(expected_values)

    return (max_score, holds[expected_values.index(max_score)])


def run_example():
    """
    Compute the dice to hold and expected score for an example hand
    """

    num_die_sides = 6
    hand = (1, 1, 1, 5, 6)
    hand_score, hold = strategy(hand, num_die_sides)
    print "Best strategy for hand", hand, "is to hold", hold, "with expected score", hand_score


# run_example()

# import poc_holds_testsuite
# poc_holds_testsuite.run_suite(gen_all_holds)
