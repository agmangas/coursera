import math

def gen_all_sequences(outcomes, length):
    """
    Iterative function that enumerates the set of all sequences of
    outcomes of given length
    """
    
    ans = set([()])
    for dummy_idx in range(length):
        temp = set()
        for seq in ans:
            for item in outcomes:
                new_seq = list(seq)
                new_seq.append(item)
                temp.add(tuple(new_seq))
        ans = temp
    return ans

# Question 1

print "Question 1"
print "=========="
print 2 ** 5

# Question 3

print
print "Question 3"
print "=========="
print "Expected value one die:", float(sum([1, 2, 3, 4])) / 4

seqs = gen_all_sequences([1,2,3,4], 2)
seqs = [tup[0] * tup[1] for tup in seqs]

print "Expected value product:", float(sum(seqs)) / len(seqs)

# Question 4

print
print "Question 4"
print "=========="

q4_total_consec = 12
q4_total_strings = 10 ** 5

print "Probability:", float(q4_total_consec) / q4_total_strings

# Question 5

print
print "Question 5"
print "=========="

q5_total_consec = 12
q5_total_strings = float(math.factorial(10)) / math.factorial(10 - 5)

print "Probability:", float(q5_total_consec) / q5_total_strings

# Question 6

print
print "Question 6"
print "=========="

def gen_permutations(outcomes, length):
    """
    Iterative function that generates set of permutations of
    outcomes of length num_trials
    No repeated outcomes allowed
    """
    
    ans = set([()])
    
    for dummy_idx in range(length):
        temp = set()
        for seq in ans:
            for item in outcomes:
                if not item in seq:
                    new_seq = list(seq)
                    new_seq.append(item)
                    temp.add(tuple(new_seq))
        ans = temp
        
    return ans

def run_example():

    # example for digits
    #outcome = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    outcome = ["Red", "Green", "Blue"]
    #outcome = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    
    length = 2
    permtutations = gen_permutations(outcome, length)
    print "Computed", len(permtutations), "permutations of length", str(length)
    print "Permutations were", permtutations

run_example()

## Final example for homework problem

outcome = set(["a", "b", "c", "d", "e", "f"])

permutations = gen_permutations(outcome, 4)
permutation_list = list(permutations)
permutation_list.sort()
print
print "Answer is", permutation_list[100]

# Question 9

print
print "Question 9"
print "=========="

q9_prob = (12.0/51.0) * (11.0/50.0) * (10.0/49.0) * (9.0/48.0)

print "Probability:", q9_prob
print "Prob. (alt):", float(5148) / float(2598960)






