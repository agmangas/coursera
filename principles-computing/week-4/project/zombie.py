"""
Student portion of Zombie Apocalypse mini-project
"""

import random
import poc_grid
import poc_queue
import poc_zombie_gui

# Global constants

EMPTY = 0
FULL = 1
FOUR_WAY = 0
EIGHT_WAY = 1
OBSTACLE = "obstacle"
HUMAN = "human"
ZOMBIE = "zombie"


class Zombie(poc_grid.Grid):

    """
    Class for simulating zombie pursuit of human on grid with
    obstacles
    """

    def __init__(self, grid_height, grid_width, obstacle_list=None,
                 zombie_list=None, human_list=None):
        """
        Create a simulation of given size with given obstacles,
        humans, and zombies
        """

        poc_grid.Grid.__init__(self, grid_height, grid_width)
        if obstacle_list != None:
            for cell in obstacle_list:
                self.set_full(cell[0], cell[1])
        if zombie_list != None:
            self._zombie_list = list(zombie_list)
        else:
            self._zombie_list = []
        if human_list != None:
            self._human_list = list(human_list)
        else:
            self._human_list = []

    def clear(self):
        """
        Set cells in obstacle grid to be empty
        Reset zombie and human lists to be empty
        """

        poc_grid.Grid.clear(self)
        self._zombie_list = list()
        self._human_list = list()

    def add_zombie(self, row, col):
        """
        Add zombie to the zombie list
        """

        self._zombie_list.append((row, col))

    def num_zombies(self):
        """
        Return number of zombies
        """

        return len(self._zombie_list)

    def zombies(self):
        """
        Generator that yields the zombies in the order they were
        added.
        """

        return (zombie for zombie in self._zombie_list)

    def add_human(self, row, col):
        """
        Add human to the human list
        """

        self._human_list.append((row, col))

    def num_humans(self):
        """
        Return number of humans
        """

        return len(self._human_list)

    def humans(self):
        """
        Generator that yields the humans in the order they were added.
        """

        return (human for human in self._human_list)

    def compute_distance_field(self, entity_type):
        """
        Function computes a 2D distance field
        Distance at member of entity_queue is zero
        Shortest paths avoid obstacles and use distance_type distances
        """

        entity_gen = self.zombies() if entity_type == ZOMBIE else self.humans()

        visited = poc_grid.Grid(self.get_grid_height(), self.get_grid_width())

        impossible_dist = self.get_grid_height() * self.get_grid_width()

        dist_field = [
            [impossible_dist for dummy_col in range(self.get_grid_width())]
            for dummy_row in range(self.get_grid_height())
        ]

        boundary = poc_queue.Queue()
        for entity in entity_gen:
            boundary.enqueue(entity)
            visited.set_full(entity[0], entity[1])
            dist_field[entity[0]][entity[1]] = int(0)

        # Loop while there are elements in the boundary queue

        while len(boundary):

            # Dequeue one cell from de boundary queue

            current = boundary.dequeue()
            current_dist = dist_field[current[0]][current[1]]

            # Loop through all the neighbors of the current cell

            for neighbor in self.four_neighbors(current[0], current[1]):

                # Visit this neighbor if it's not an obstacle

                if self.is_empty(neighbor[0], neighbor[1]):

                    # Visit this neighbor if we have not done it yet

                    if visited.is_empty(neighbor[0], neighbor[1]):

                        # Enqueue for future dequeueing

                        visited.set_full(neighbor[0], neighbor[1])
                        boundary.enqueue(neighbor)

                        # Update distance

                        neighbor_dist = dist_field[neighbor[0]][neighbor[1]]
                        new_dist = min((current_dist + 1, neighbor_dist))
                        dist_field[neighbor[0]][neighbor[1]] = new_dist

        return dist_field

    def move_humans(self, zombie_distance):
        """
        Function that moves humans away from zombies, diagonal moves
        are allowed
        """

        self._human_list[:] = [
            self.optimize_dist(
                human, zombie_distance,
                distance_func=max, eight_neighbors=True
            ) for human in self.humans()
        ]

    def move_zombies(self, human_distance):
        """
        Function that moves zombies towards humans, no diagonal moves
        are allowed
        """

        self._zombie_list[:] = [
            self.optimize_dist(
                zombie, human_distance,
                distance_func=min, eight_neighbors=False
            ) for zombie in self.zombies()
        ]

    def optimize_dist(self, grid_coord, distance_field,
                      distance_func=max, eight_neighbors=False):
        """
        Returns the move that optimizes distance starting
        from grid_coord according to distance_field and using
        distance_func as the distance decision function
        """

        neigh_func = self.eight_neighbors if eight_neighbors else self.four_neighbors

        neighbors = [
            neigh for neigh in neigh_func(grid_coord[0], grid_coord[1])
            if self.is_empty(neigh[0], neigh[1])
        ]

        search_coords = neighbors + [grid_coord]

        distances = [
            distance_field[coord[0]][coord[1]]
            for coord in search_coords
        ]

        return search_coords[distances.index(distance_func(distances))]


# Start up gui for simulation - You will need to write some code above
# before this will work without errors

# poc_zombie_gui.run_gui(Zombie(30, 40))
