"""
Loyd's Fifteen puzzle - solver and visualizer
Note that solved configuration has the blank (zero) tile in upper left
Use the arrows key to swap this tile with its neighbors
"""

import poc_fifteen_gui


class Puzzle:

    """
    Class representation for the Fifteen puzzle
    """

    def __init__(self, puzzle_height, puzzle_width, initial_grid=None):
        """
        Initialize puzzle with default height and width
        Returns a Puzzle object
        """

        self._height = puzzle_height
        self._width = puzzle_width
        self._grid = [[col + puzzle_width * row
                       for col in range(self._width)]
                      for row in range(self._height)]

        if initial_grid != None:
            for row in range(puzzle_height):
                for col in range(puzzle_width):
                    self._grid[row][col] = initial_grid[row][col]

    def __str__(self):
        """
        Generate string representaion for puzzle
        Returns a string
        """

        ans = ""
        for row in range(self._height):
            ans += str(self._grid[row])
            ans += "\n"
        return ans

    # GUI methods

    def get_height(self):
        """
        Getter for puzzle height
        Returns an integer
        """

        return self._height

    def get_width(self):
        """
        Getter for puzzle width
        Returns an integer
        """

        return self._width

    def get_number(self, row, col):
        """
        Getter for the number at tile position pos
        Returns an integer
        """

        return self._grid[row][col]

    def set_number(self, row, col, value):
        """
        Setter for the number at tile position pos
        """

        self._grid[row][col] = value

    def clone(self):
        """
        Make a copy of the puzzle to update during solving
        Returns a Puzzle object
        """

        new_puzzle = Puzzle(self._height, self._width, self._grid)
        return new_puzzle

    # Core puzzle methods

    def current_position(self, solved_row, solved_col):
        """
        Locate the current position of the tile that will be at
        position (solved_row, solved_col) when the puzzle is solved
        Returns a tuple of two integers        
        """

        solved_value = (solved_col + self._width * solved_row)

        for row in range(self._height):
            for col in range(self._width):
                if self._grid[row][col] == solved_value:
                    return (row, col)
        assert False, "Value " + str(solved_value) + " not found"

    def update_puzzle(self, move_string):
        """
        Updates the puzzle state based on the provided move string
        """

        zero_row, zero_col = self.current_position(0, 0)
        for direction in move_string:
            if direction == "l":
                assert zero_col > 0, "move off grid: " + direction
                self._grid[zero_row][zero_col] = self._grid[zero_row][zero_col - 1]
                self._grid[zero_row][zero_col - 1] = 0
                zero_col -= 1
            elif direction == "r":
                assert zero_col < self._width - 1, "move off grid: " + direction
                self._grid[zero_row][zero_col] = self._grid[zero_row][zero_col + 1]
                self._grid[zero_row][zero_col + 1] = 0
                zero_col += 1
            elif direction == "u":
                assert zero_row > 0, "move off grid: " + direction
                self._grid[zero_row][zero_col] = self._grid[zero_row - 1][zero_col]
                self._grid[zero_row - 1][zero_col] = 0
                zero_row -= 1
            elif direction == "d":
                assert zero_row < self._height - 1, "move off grid: " + direction
                self._grid[zero_row][zero_col] = self._grid[zero_row + 1][zero_col]
                self._grid[zero_row + 1][zero_col] = 0
                zero_row += 1
            else:
                assert False, "invalid direction: " + direction

    # Helper methods

    def is_ok(self, row, col):
        """
        Checks if the tile at (row, col) is the correct one
        """

        return self.current_position(row, col) == (row, col)

    def next_tile_right(self, row, col):
        """
        Returns the next tile to the right from the tile passed as parameter
        """

        assert row < self._height
        assert col < self._width

        if row == self._height - 1 and col == self._width - 1:
            return None
        elif col == self._width - 1:
            return (row + 1, 0)
        else:
            return (row, col + 1)

    def tiles_it(self, start_row, start_col):
        """
        Generator that iterates over the tiles of the puzzle
        """

        assert start_row < self._height and start_col < self._width

        next_tile = start_row, start_col

        while next_tile is not None:

            yield next_tile
            next_tile = self.next_tile_right(next_tile[0], next_tile[1])

    # Phase one methods

    def _solve_interior_up(self, target_row, target_col):
        """
        Solves the puzzle for an interior row when the correct tile that 
        will be at target position is in the same col but a higher row:

        *-*-*-*
        |-|7|-|
        *-*-*-*
        |-|-|-|
        *-*-*-*
        |-|0|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(target_row, target_col)

        assert self.lower_row_invariant(target_row, target_col)
        assert curr_pos[1] == target_col
        assert curr_pos[0] < target_row
        assert target_col > 0

        moves = list()
        clone = self.clone()

        moves.append("u" * (target_row - curr_pos[0]))
        clone.update_puzzle(moves[-1])

        while not clone.is_ok(target_row, target_col):
            moves.append("lddru")
            clone.update_puzzle(moves[-1])

        moves.append("ld")
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def _solve_interior_right(self, target_row, target_col):
        """
        Solves the puzzle for an interior row when the correct tile that 
        will be at target position is to the right of the target position:

        *-*-*-*
        |-|-|-|
        *-*-*-*
        |-|-|7|
        *-*-*-*
        |-|0|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(target_row, target_col)

        assert self.lower_row_invariant(target_row, target_col)
        assert curr_pos[1] > target_col
        assert curr_pos[0] < target_row
        assert target_col > 0

        moves = list()
        clone = self.clone()

        moves.append("u" * (target_row - curr_pos[0]))
        clone.update_puzzle(moves[-1])

        moves.append("r" * (curr_pos[1] - target_col))
        clone.update_puzzle(moves[-1])

        while clone.current_position(target_row, target_col)[1] != target_col:
            moves.append("ulldr" if not curr_pos[0] == 0 else "dllur")
            clone.update_puzzle(moves[-1])

        moves.append("ul" if not curr_pos[0] == 0 else "dlu")
        clone.update_puzzle(moves[-1])

        while not clone.is_ok(target_row, target_col):
            moves.append("lddru")
            clone.update_puzzle(moves[-1])

        moves.append("ld")
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def _solve_interior_left(self, target_row, target_col):
        """
        Solves the puzzle for an interior row when the correct tile that 
        will be at target position is to the left of the target position:

        *-*-*-*
        |-|-|-|
        *-*-*-*
        |7|-|-|
        *-*-*-*
        |-|0|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(target_row, target_col)

        assert self.lower_row_invariant(target_row, target_col)
        assert curr_pos[1] < target_col
        assert curr_pos[0] <= target_row
        assert target_col > 0

        moves = list()
        clone = self.clone()

        moves.append("u" * (target_row - curr_pos[0]))
        clone.update_puzzle(moves[-1])

        moves.append("l" * (target_col - curr_pos[1]))
        clone.update_puzzle(moves[-1])

        if self.lower_row_invariant(target_row, target_col - 1):
            return "".join(moves)

        while clone.current_position(target_row, target_col)[1] != target_col:
            moves.append("urrdl" if not curr_pos[0] == 0 else "drrul")
            clone.update_puzzle(moves[-1])

        moves.append("ur" if not curr_pos[0] == 0 else "dru")
        clone.update_puzzle(moves[-1])

        while not clone.is_ok(target_row, target_col):
            moves.append("lddru")
            clone.update_puzzle(moves[-1])

        moves.append("ld")
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def _solve_col0_far_right(self, target_row):
        """
        Solves the puzzle for col0 when the correct tile that 
        will be at target position is at least at col 2:

        *-*-*-*
        |-|-|-|
        *-*-*-*
        |-|-|6|
        *-*-*-*
        |0|7|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(target_row, 0)

        assert self.lower_row_invariant(target_row, 0)
        assert curr_pos[1] > 1
        assert curr_pos[0] < target_row

        moves = list()
        clone = self.clone()

        moves.append("u" * (target_row - curr_pos[0]))
        clone.update_puzzle(moves[-1])

        moves.append("r" * (curr_pos[1]))
        clone.update_puzzle(moves[-1])

        while clone.current_position(target_row, 0)[1] != 1:
            moves.append("ulldr" if not curr_pos[0] == 0 else "dllur")
            clone.update_puzzle(moves[-1])

        if clone.current_position(target_row, 0)[0] == target_row - 1:
            moves.append("ulld")
            clone.update_puzzle(moves[-1])
            return "".join(moves)

        moves.append("dlu")
        clone.update_puzzle(moves[-1])

        while clone.current_position(target_row, 0)[0] != target_row - 1:
            moves.append("lddru")
            clone.update_puzzle(moves[-1])

        moves.append("ld")
        clone.update_puzzle(moves[-1])

        moves.append("ruldrdlurdluurddlu" + ("r" * (self._width - 1)))
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def _solve_col0_right(self, target_row):
        """
        Solves the puzzle for col0 when the correct tile that 
        will be at target position is at col 1:

        *-*-*-*
        |-|-|-|
        *-*-*-*
        |-|6|-|
        *-*-*-*
        |0|7|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(target_row, 0)

        assert self.lower_row_invariant(target_row, 0)
        assert curr_pos[1] == 1
        assert curr_pos[0] < target_row

        moves = list()
        clone = self.clone()

        if clone.current_position(target_row, 0)[0] == target_row - 1:
            moves.append("u")
            clone.update_puzzle(moves)
        else:
            moves.append("u" * (target_row - curr_pos[0] - 1))
            clone.update_puzzle(moves[-1])

            moves.append("ru")
            clone.update_puzzle(moves[-1])

            while clone.current_position(target_row, 0)[0] != target_row - 1:
                moves.append("lddru")
                clone.update_puzzle(moves[-1])

            moves.append("ld")
            clone.update_puzzle(moves[-1])

        moves.append("ruldrdlurdluurddlu" + ("r" * (self._width - 1)))
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def _solve_col0_up(self, target_row):
        """
        Solves the puzzle for col0 when the correct tile that 
        will be at target position is at col 0:

        *-*-*-*
        |-|-|-|
        *-*-*-*
        |6|-|-|
        *-*-*-*
        |0|7|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(target_row, 0)

        assert self.lower_row_invariant(target_row, 0)
        assert curr_pos[1] == 0
        assert curr_pos[0] < target_row

        moves = list()
        clone = self.clone()

        if clone.current_position(target_row, 0)[0] == target_row - 1:
            moves.append("u" + ("r" * (self._width - 1)))
            clone.update_puzzle(moves[-1])
            return "".join(moves)

        moves.append("u" * (target_row - curr_pos[0] - 1))
        clone.update_puzzle(moves[-1])

        moves.append("ruldru")
        clone.update_puzzle(moves[-1])

        while clone.current_position(target_row, 0)[0] != target_row - 1:
            moves.append("lddru")
            clone.update_puzzle(moves[-1])

        moves.append("ld")
        clone.update_puzzle(moves[-1])

        moves.append("ruldrdlurdluurddlu" + ("r" * (self._width - 1)))
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def lower_row_invariant(self, target_row, target_col):
        """
        Check whether the puzzle satisfies the specified invariant
        at the given position in the bottom rows of the puzzle (target_row > 1)
        Returns a boolean
        """

        tiles, row, col = list(), target_row, target_col

        while True:

            tiles.append(self.get_number(row, col))
            next_tile = self.next_tile_right(row, col)

            if next_tile is None:
                break
            else:
                row, col = next_tile[0], next_tile[1]

        if not tiles.pop(0) == 0:
            return False

        if len(tiles) < 2:
            return True

        diff = [tiles[idx + 1] - tiles[idx] for idx in xrange(len(tiles) - 1)]

        if len(set(diff)) != 1 or diff[0] != 1:
            return False

        return True

    def solve_interior_tile(self, target_row, target_col):
        """
        Place correct tile at target position
        Updates puzzle and returns a move string
        """

        curr_pos = self.current_position(target_row, target_col)

        solve_cases = {
            curr_pos[1] == target_col: self._solve_interior_up,
            curr_pos[1] > target_col: self._solve_interior_right,
            curr_pos[1] < target_col: self._solve_interior_left
        }

        move = solve_cases[True](target_row, target_col)
        self.update_puzzle(move)

        assert self.lower_row_invariant(target_row, target_col - 1)

        return move

    def solve_col0_tile(self, target_row):
        """
        Solve tile in column zero on specified row (> 1)
        Updates puzzle and returns a move string
        """

        curr_pos = self.current_position(target_row, 0)

        solve_cases = {
            curr_pos[1] > 1: self._solve_col0_far_right,
            curr_pos[1] == 1: self._solve_col0_right,
            curr_pos[1] == 0: self._solve_col0_up
        }

        move = solve_cases[True](target_row)
        self.update_puzzle(move)

        if target_row > 2:
            assert self.lower_row_invariant(target_row - 1, self._width - 1)

        return move

    # Phase two methods

    def _solve_row1_up_row(self, target_col):
        """
        Solves the puzzle for row1 when the correct tile that 
        will be at target position is at row 0:

        *-*-*-*
        |5|-|-|
        *-*-*-*
        |-|-|0|
        *-*-*-*
        |6|7|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(1, target_col)

        moves = list()
        clone = self.clone()

        moves.append("u")
        clone.update_puzzle(moves[-1])

        if clone.is_ok(1, target_col):
            return "".join(moves)

        moves.append("l" * (target_col - curr_pos[1]))
        clone.update_puzzle(moves[-1])

        while clone.current_position(1, target_col)[1] != target_col:
            moves.append("drrul")
            clone.update_puzzle(moves[-1])

        moves.append("dru")
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def _solve_row1_same_row(self, target_col):
        """
        Solves the puzzle for row1 when the correct tile that 
        will be at target position is at row 1:

        *-*-*-*
        |-|-|-|
        *-*-*-*
        |5|-|0|
        *-*-*-*
        |6|7|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(1, target_col)

        moves = list()
        clone = self.clone()

        moves.append("l" * (target_col - curr_pos[1]))
        clone.update_puzzle(moves[-1])

        if clone.is_ok(1, target_col):
            moves.append("ur")
            clone.update_puzzle(moves[-1])
            return "".join(moves)

        while clone.current_position(1, target_col)[1] != target_col:
            moves.append("urrdl")
            clone.update_puzzle(moves[-1])

        moves.append("ur")
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def _solve_row0_same_row(self, target_col):
        """
        Solves the puzzle for row0 when the correct tile that 
        will be at target position is at row 0:

        *-*-*-*
        |2|-|0|
        *-*-*-*
        |-|-|5|
        *-*-*-*
        |6|7|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(0, target_col)

        moves = list()
        clone = self.clone()

        moves.append("ld")
        clone.update_puzzle(moves[-1])

        if clone.is_ok(0, target_col):
            return "".join(moves)

        moves.append("u" + "l" * (target_col - curr_pos[1] - 1))
        clone.update_puzzle(moves[-1])

        while clone.current_position(0, target_col)[1] != target_col - 1:
            moves.append("drrul")
            clone.update_puzzle(moves[-1])

        moves.append("druld")
        clone.update_puzzle(moves[-1])

        moves.append("urdlurrdluldrruld")
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def _solve_row0_down_row(self, target_col):
        """
        Solves the puzzle for row0 when the correct tile that 
        will be at target position is at row 1:

        *-*-*-*
        |-|-|0|
        *-*-*-*
        |2|-|5|
        *-*-*-*
        |6|7|8|
        *-*-*-*

        Returns the correct moves
        This does not modify the puzzle
        """

        curr_pos = self.current_position(0, target_col)

        moves = list()
        clone = self.clone()

        moves.append("l")
        clone.update_puzzle(moves[-1])

        if clone.current_position(0, target_col)[1] == target_col - 1:
            moves.append("ld")
            clone.update_puzzle(moves[-1])
        else:
            moves.append("d" + "l" * (target_col - curr_pos[1] - 1))
            clone.update_puzzle(moves[-1])

            while clone.current_position(0, target_col)[1] != target_col - 1:
                moves.append("urrdl")
                clone.update_puzzle(moves[-1])

        moves.append("urdlurrdluldrruld")
        clone.update_puzzle(moves[-1])

        return "".join(moves)

    def row0_invariant(self, target_col):
        """
        Check whether the puzzle satisfies the row zero invariant
        at the given column (col > 1)
        Returns a boolean
        """

        if not self.get_number(0, target_col) == 0:
            return False

        if self._height > 2:

            clone = self.clone()
            clone.set_number(2, 0, 0)

            if not clone.lower_row_invariant(2, 0):
                return False

        if not self.is_ok(1, target_col):
            return False

        must_be_ok = [(1, col) for col in range(target_col + 1, self._width)]
        must_be_ok += [(0, col) for col in range(target_col + 1, self._width)]

        if False in (self.is_ok(row, col) for row, col in must_be_ok):
            return False

        return True

    def row1_invariant(self, target_col):
        """
        Check whether the puzzle satisfies the row one invariant
        at the given column (col > 1)
        Returns a boolean
        """

        if not self.get_number(1, target_col) == 0:
            return False

        if self._height > 2:

            clone = self.clone()
            clone.set_number(2, 0, 0)

            if not clone.lower_row_invariant(2, 0):
                return False

        must_be_ok = [(1, col) for col in range(target_col + 1, self._width)]

        if False in (self.is_ok(row, col) for row, col in must_be_ok):
            return False

        return True

    def solve_row0_tile(self, target_col):
        """
        Solve the tile in row zero at the specified column
        Updates puzzle and returns a move string
        """

        curr_pos = self.current_position(0, target_col)

        solve_cases = {
            curr_pos[0] == 1: self._solve_row0_down_row,
            curr_pos[0] == 0: self._solve_row0_same_row
        }

        move = solve_cases[True](target_col)
        self.update_puzzle(move)

        assert self.row1_invariant(target_col - 1)

        return move

    def solve_row1_tile(self, target_col):
        """
        Solve the tile in row one at the specified column
        Updates puzzle and returns a move string
        """

        curr_pos = self.current_position(1, target_col)

        solve_cases = {
            curr_pos[0] == 0: self._solve_row1_up_row,
            curr_pos[0] == 1: self._solve_row1_same_row
        }

        move = solve_cases[True](target_col)
        self.update_puzzle(move)

        assert self.row0_invariant(target_col)

        return move

    # Phase 3 methods

    def solve_2x2(self):
        """
        Solve the upper left 2x2 part of the puzzle
        Updates the puzzle and returns a move string
        """

        assert self.row1_invariant(1)

        moves = list()

        moves.append("lu")
        self.update_puzzle(moves[-1])

        while not self.row0_invariant(0):
            moves.append("rdlu")
            self.update_puzzle(moves[-1])

        return "".join(moves)

    def solve_puzzle(self):
        """
        Generate a solution string for a puzzle
        Updates the puzzle and returns a move string
        """

        moves = list()

        # Move zero to the last tile

        ini_zero_pos = self.current_position(0, 0)

        moves.append("d" * (self._height - ini_zero_pos[0] - 1))
        self.update_puzzle(moves[-1])

        moves.append("r" * (self._width - ini_zero_pos[1] - 1))
        self.update_puzzle(moves[-1])

        assert self.lower_row_invariant(self._height - 1, self._width - 1)

        # Solve interior tiles

        if self._height > 2:

            interior_tiles = list(self.tiles_it(2, 0))
            interior_tiles.reverse()

            for tile in interior_tiles:

                assert self.lower_row_invariant(tile[0], tile[1])

                if tile[1] > 0:
                    moves.append(self.solve_interior_tile(tile[0], tile[1]))
                else:
                    moves.append(self.solve_col0_tile(tile[0]))

        # Solve row0 and row1 tiles

        row0_row1_cols = range(2, self._width)
        row0_row1_cols.reverse()

        for col in row0_row1_cols:

            assert self.row1_invariant(col)
            moves.append(self.solve_row1_tile(col))

            assert self.row0_invariant(col)
            moves.append(self.solve_row0_tile(col))

        # Solve last 2x2 puzzle

        moves.append(self.solve_2x2())

        return "".join(moves)

# Start interactive simulation

# poc_fifteen_gui.FifteenGUI(Puzzle(4, 4))
