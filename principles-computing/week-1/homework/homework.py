"""
Simulator for resource generation with upgrades
"""

import simpleplot
import math
import codeskulptor

codeskulptor.set_timeout(20)


def resources_vs_time(upgrade_cost_increment, num_upgrade):
    """
    Build function that performs unit upgrades with specified cost increments
    """

    ret = list()

    current_upgrade_cost = float(1)
    current_time = float(0)
    resources_per_second = float(1)
    total_resources_generated = float(0)

    for idx in xrange(num_upgrade):

        secs_wait = float(current_upgrade_cost) / resources_per_second
        current_time += secs_wait
        total_resources_generated += resources_per_second * secs_wait
        resources_per_second += 1
        current_upgrade_cost += upgrade_cost_increment

        ret.append([current_time, total_resources_generated])

    return ret


def resources_vs_time_percent_inc(upgrade_cost_factor_increment, num_upgrade):
    """
    Build function that performs unit upgrades with specified cost increments
    """

    ret = list()

    current_upgrade_cost = float(1)
    current_time = float(0)
    resources_per_second = float(1)
    total_resources_generated = float(0)

    for idx in xrange(num_upgrade):

        secs_wait = float(current_upgrade_cost) / resources_per_second
        current_time += secs_wait
        total_resources_generated += resources_per_second * secs_wait
        resources_per_second += 1
        current_upgrade_cost = current_upgrade_cost * upgrade_cost_factor_increment

        ret.append([current_time, total_resources_generated])

    return ret


def test():
    """
    Testing code for resources_vs_time
    """

    data1 = resources_vs_time(0.5, 20)
    data2 = resources_vs_time(1.5, 10)

    expected_data1 = [
        [1.0, 1], [1.75, 2.5], [2.41666666667, 4.5], [3.04166666667, 7.0],
        [3.64166666667, 10.0], [4.225, 13.5], [4.79642857143, 17.5],
        [5.35892857143, 22.0], [5.91448412698, 27.0], [6.46448412698, 32.5],
        [7.00993867244, 38.5], [7.55160533911, 45.0], [8.09006687757, 52.0],
        [8.62578116328, 59.5], [9.15911449661, 67.5], [9.69036449661, 76.0],
        [10.2197762613, 85.0], [10.7475540391, 94.5], [11.2738698286, 104.5],
        [11.7988698286, 115.0]
    ]

    expected_data2 = [
        [1.0, 1], [2.25, 3.5], [3.58333333333, 7.5], [4.95833333333, 13.0],
        [6.35833333333, 20.0], [7.775, 28.5], [9.20357142857, 38.5], [10.6410714286, 50.0],
        [12.085515873, 63.0], [13.535515873, 77.5]
    ]

    print "data1"
    print "====="
    print data1

    print "data2"
    print "====="
    print data2

    print "Data1 Comparison"
    print "================"

    for idx in xrange(len(data1)):
        print data1[idx]
        print expected_data1[idx]

    print "Data2 Comparison"
    print "================"

    for idx in xrange(len(data2)):
        print data2[idx]
        print expected_data2[idx]

    simpleplot.plot_lines("Growth", 600, 600, "time", "total resources", [data1, data2])


def question_1():

    answer_1 = resources_vs_time(0.0, 10)
    answer_2 = resources_vs_time(1.0, 10)

    print "answer_1"
    print "========"
    print answer_1

    print "answer_2"
    print "========"
    print answer_2

    print "Answer Question 1"
    print "================="
    print answer_1[-1][1], answer_2[-1][1]


def question_2(num_upgrades=20):

    upgrade_cost_increments = [0, 0.5, 1, 2]

    results = []

    for cost in upgrade_cost_increments:
        results.append(resources_vs_time(cost, num_upgrades))

    simpleplot.plot_lines(
        "Question 2", 600, 600, "Time (secs)", "Total Resources",
        results, True, upgrade_cost_increments
    )


def question_3(num_upgrades=int(10e3)):

    result = resources_vs_time(0, num_upgrades)
    result = map(lambda elem: [math.log(elem[0]), math.log(elem[1])], result)

    simpleplot.plot_lines(
        "Question 3", 600, 600, "Log Time (secs)", "Log Total Resources", [result]
    )


def question_4(num_upgrades=10):

    result = resources_vs_time(0, num_upgrades)

    print "result"
    print "======"
    for val in result:
        print val


def question_5():

    test_funcs = {
        "logn": lambda x: math.log(x),
        "lin": lambda x: x,
        "quad": lambda x: 0.5 * x * (x + 1),
        "const": lambda x: int(2)
    }

    test_num_upgrades = [10, 100, 1000, 10000]

    results = dict()

    for num_upgrades in test_num_upgrades:

        results[num_upgrades] = dict()

        for func_key in test_funcs:

            result = resources_vs_time(0, num_upgrades)
            total_time = result[-1][0]
            results[num_upgrades][func_key] = abs(test_funcs[func_key](num_upgrades) - total_time)

    print "results"
    print "======="
    for num_upgrades in results:
        print "Num Upgrades: %s" % num_upgrades
        print results[num_upgrades]


def question_7(num_upgrades=20):

    result = resources_vs_time(1, num_upgrades)
    result = map(lambda elem: [math.log(elem[0]), math.log(elem[1])], result)

    slope = float(result[-1][1] - result[0][1]) / float(result[-1][0] - result[0][0])

    simpleplot.plot_lines(
        "Question 7", 600, 600, "Log Time (secs)", "Log Total Resources", [result]
    )

    print "slope"
    print "====="
    print slope


def question_8(num_upgrades=10):

    result = resources_vs_time(1, num_upgrades)

    print "result"
    print "======"
    for val in result:
        print val


def question_10(num_upgrades=10):

    result = resources_vs_time_percent_inc(1.15, num_upgrades)

    print "result"
    print "======"
    for val in result:
        print val


def question_11(num_upgrades=50):

    results = []

    results.append(resources_vs_time(1, num_upgrades))
    results.append(resources_vs_time_percent_inc(1.15, num_upgrades))

    simpleplot.plot_lines(
        "Question 11", 600, 600, "Time (secs)", "Total Resources",
        results, True, ["Constant", "Cookie Clicker"]
    )

    independent_constant_plot = resources_vs_time(1, int(num_upgrades * 3.5))

    simpleplot.plot_lines(
        "Question 11 (Constant)", 600, 600, "Time (secs)", "Total Resources",
        [independent_constant_plot], True
    )

    simpleplot.plot_lines(
        "Question 11 (Cookie Clicker)", 600, 600, "Time (secs)", "Total Resources",
        [results[1]], True
    )

if __name__ == "__main__":

    execute_functions = [
        test, question_1, question_2, question_3,
        question_4, question_5, question_7, question_8,
        question_10, question_11
    ]

    for func in execute_functions:

        print
        print "***** %s" % (str(func))
        print

        func()
