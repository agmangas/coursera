"""
Cookie Clicker Simulator
"""

import math
import simpleplot
import poc_clicker_provided as provided

# Used to increase the timeout, if necessary

import codeskulptor
codeskulptor.set_timeout(20)

# Constants

SIM_TIME = 10000000000.0


class ClickerState:

    """
    Simple class to keep track of the game state.
    """

    def __init__(self):
        """
        Constructor
        """

        self._total_produced_cookies = float(0)
        self._current_cookies = float(0)
        self._current_time = float(0)
        self._current_cps = float(1)

        # History format:
        # [time, item bought at time (or None), item cost, total produced cookies]

        self._history = [(float(0), None, float(0), float(0))]

    def __str__(self):
        """
        Return human readable state
        """

        ret = str()

        ret += "\nClickerState Photo\n"
        ret += "====================\n"
        ret += "total_produced_cookies = %s\n" % (self._total_produced_cookies)
        ret += "current_cookies = %s\n" % (self._current_cookies)
        ret += "current_time = %s\n" % (self._current_time)
        ret += "current_cps = %s\n" % (self._current_cps)

        return ret

    def _append_history_entry(self, item, item_cost):
        """
        Appends a new history entry given the current 
        state of the class and item info
        """

        self._history.append((
            self.get_time(),
            item,
            item_cost,
            self.get_total_produced_cookies()
        ))

    def get_total_produced_cookies(self):
        """
        Return total number of produced cookies
        """

        return self._total_produced_cookies

    def get_cookies(self):
        """
        Return current number of cookies 
        (not total number of cookies)
        
        Should return a float
        """

        return self._current_cookies

    def get_cps(self):
        """
        Get current CPS

        Should return a float
        """

        return self._current_cps

    def get_time(self):
        """
        Get current time

        Should return a float
        """

        return self._current_time

    def get_history(self):
        """
        Return history list

        History list should be a list of tuples of the form:
        (time, item, cost of item, total cookies)

        For example: (0.0, None, 0.0, 0.0)
        """

        return self._history

    def time_until(self, cookies):
        """
        Return time until you have the given number of cookies
        (could be 0 if you already have enough cookies)

        Should return a float with no fractional part
        """

        if self.get_cookies() >= cookies:
            return float(0)

        cookies_diff = cookies - self.get_cookies()

        return math.ceil(float(cookies_diff) / self.get_cps())

    def wait(self, time):
        """
        Wait for given amount of time and update state

        Should do nothing if time <= 0
        """

        if time <= 0:
            return None

        cookies_produced = self.get_cps() * time

        self._current_time += time
        self._current_cookies += cookies_produced
        self._total_produced_cookies += cookies_produced

    def buy_item(self, item_name, cost, additional_cps):
        """
        Buy an item and update state

        Should do nothing if you cannot afford the item
        """

        if cost > self.get_cookies():
            return None

        self._current_cookies -= cost
        self._current_cps += additional_cps

        self._append_history_entry(item_name, cost)


def simulate_clicker(build_info, duration, strategy):
    """
    Function to run a Cookie Clicker game for the given
    duration with the given strategy.  Returns a ClickerState
    object corresponding to game.
    """

    build_info_clone = build_info.clone()

    clicker_state = ClickerState()

    # Check the current time and break out of the loop if the duration has been passed

    while clicker_state.get_time() <= duration:

        time_left = duration - clicker_state.get_time()

        # Determine which item to purchase next

        strategy_selected_item = strategy(
            clicker_state.get_cookies(),
            clicker_state.get_cps(),
            time_left,
            build_info_clone
        )

        # If the strategy function returns None, break out of the loop

        if strategy_selected_item is None:
            break

        # Get the cost and CPS of the item

        item_cost = build_info_clone.get_cost(strategy_selected_item)
        item_cps = build_info_clone.get_cps(strategy_selected_item)

        # Determine how much time must elapse until it is possible to purchase the item

        needed_wait_time = clicker_state.time_until(item_cost)

        # Do not allow the simulation to run past the duration

        if needed_wait_time > time_left:
            break

        # Wait until that time

        clicker_state.wait(needed_wait_time)

        # Buy the item

        clicker_state.buy_item(strategy_selected_item, item_cost, item_cps)

        # Update the build information

        build_info_clone.update_item(strategy_selected_item)

    # If there is time left, allow cookies to accumulate

    remainder_time = duration - clicker_state.get_time()

    if remainder_time > 0:
        clicker_state.wait(remainder_time)

    return clicker_state


def strategy_cursor(cookies, cps, time_left, build_info):
    """
    Always pick Cursor!

    Note that this simplistic strategy does not properly check whether
    it can actually buy a Cursor in the time left.  Your strategy
    functions must do this and return None rather than an item you
    can't buy in the time left.
    """

    return "Cursor"


def strategy_none(cookies, cps, time_left, build_info):
    """
    Always return None

    This is a pointless strategy that you can use to help debug
    your simulate_clicker function.
    """

    return None


def _strategy(dec_func, cookies, cps, time_left, build_info):
    """
    Always select an item based on a decision function.

    This cost decision function should have the following signature.

        dec_func(cookies, cps, time_left, build_info)

    It should return the name of the item or None if it cannot decide on one.
    """

    selected_item = dec_func(cookies, cps, time_left, build_info)

    if selected_item is None:
        return None

    return selected_item


def _cost_cheap(cookies, cps, time_left, build_info):
    """
    Cost function that always selects the cheapest element
    """

    list_names = build_info.build_items()
    list_costs = [build_info.get_cost(name) for name in list_names]

    future_cookies = cookies + (time_left * cps)
    filtered_costs = [cost for cost in list_costs if cost <= future_cookies]

    if not len(filtered_costs):
        return None

    return list_names[list_costs.index(min(filtered_costs))]


def strategy_cheap(cookies, cps, time_left, build_info):
    """
    Always select the cheapest item
    """

    return _strategy(_cost_cheap, cookies, cps, time_left, build_info)


def _cost_expensive(cookies, cps, time_left, build_info):
    """
    Cost function that always selects the most expensive element
    """

    list_names = build_info.build_items()
    list_costs = [build_info.get_cost(name) for name in list_names]

    future_cookies = cookies + (time_left * cps)
    filtered_costs = [cost for cost in list_costs if cost <= future_cookies]

    if not len(filtered_costs):
        return None

    return list_names[list_costs.index(max(filtered_costs))]


def strategy_expensive(cookies, cps, time_left, build_info):
    """
    Always the most expensive item
    """

    return _strategy(_cost_expensive, cookies, cps, time_left, build_info)


def _cost_cpscost_ratio(cookies, cps, time_left, build_info):
    """
    Cost function that always selects the item with the best cps / cost ratio
    """

    future_cookies = cookies + (time_left * cps)

    # Filter items that are out of our possibilities

    valid = [
        name for name in build_info.build_items()
        if build_info.get_cost(name) <= future_cookies
    ]

    if not len(valid):
        return None

    # Build a dict of dicts where the keys are the item names

    info = dict()

    for name in valid:
        info[name] = dict()
        info[name]["cost"] = build_info.get_cost(name)
        info[name]["cps"] = build_info.get_cps(name)
        info[name]["ratio"] = float(info[name]["cps"]) / info[name]["cost"]

    # Get the item with the highest ratio

    info_items = info.items()
    info_ratios = [item[1]["ratio"] for item in info_items]
    max_ratio_idx = info_ratios.index(max(info_ratios))

    return info_items[max_ratio_idx][0]


def strategy_best(cookies, cps, time_left, build_info):
    """
    The best strategy I could come up with
    """

    return _strategy(_cost_cpscost_ratio, cookies, cps, time_left, build_info)


def run_strategy(strategy_name, time, strategy):
    """
    Run a simulation with one strategy
    """

    state = simulate_clicker(provided.BuildInfo(), time, strategy)

    print strategy_name, ":", state

    # Plot total cookies over time
    # Uncomment out the lines below to see a plot of total cookies vs. time
    # Be sure to allow popups, if you do want to see it

    # history = state.get_history()
    # history = [(item[0], item[3]) for item in history]
    # simpleplot.plot_lines(strategy_name, 1000, 400, 'Time', 'Total Cookies', [history], True)


def run():
    """
    Run the simulator.
    """

    run_strategy("Cursor", SIM_TIME, strategy_cursor)
    run_strategy("Cheap", SIM_TIME, strategy_cheap)
    run_strategy("Expensive", SIM_TIME, strategy_expensive)
    run_strategy("Best", SIM_TIME, strategy_best)

# run()
