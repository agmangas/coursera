"""
Monte Carlo Tic-Tac-Toe Player
"""

import random
import poc_ttt_gui
import poc_ttt_provided as provided

# Constants for Monte Carlo simulator
# Change as desired

NTRIALS = 30  # Number of trials to run
MCMATCH = 3.0  # Score for squares played by the machine player
MCOTHER = 1.5  # Score for squares played by the other player

# These constants represent the result of a TicTacToe
# match from the point of view of a given player

MR_DRAW = "MR_DRAW"
MR_REFERENCE_WON = "MR_REFERENCE_WON"
MR_OTHER_WON = "MR_OTHER_WON"


def _find_match_result(board, player):
    """
    Fin the match result.
    """

    results = {
        provided.DRAW: MR_DRAW,
        player: MR_REFERENCE_WON,
        provided.switch_player(player): MR_OTHER_WON
    }

    return results[board.check_win()]


def mc_trial(board, player):
    """
    This function takes a current board and the next player to move. 

    The function should play a game starting with the given player by 
    making random moves, alternating between players. 

    The function should return when the game is over. 

    The modified board will contain the state of the game, so the 
    function does not return anything.
    """

    current_player = player

    while board.check_win() is None:
        selected_square = random.choice(board.get_empty_squares())
        board.move(selected_square[0], selected_square[1], current_player)
        current_player = provided.switch_player(current_player)


def _get_square_score_draw(square, board, player):
    """
    Get score of board square assuming that the result is a draw.
    """

    return float(0)


def _get_square_score_reference_won(square, board, player):
    """
    Get score of board square assuming that the reference player won.
    """

    if square == player:
        return MCMATCH
    elif square == provided.switch_player(player):
        return -MCOTHER
    else:
        return float(0)


def _get_square_score_other_won(square, board, player):
    """
    Get score of board square assuming that the other player won 
    (not the reference one).
    """

    if square == player:
        return -MCMATCH
    elif square == provided.switch_player(player):
        return MCOTHER
    else:
        return float(0)


def mc_update_scores(scores, board, player):
    """
    This function takes a grid of scores (a list of lists) with the same 
    dimensions as the Tic-Tac-Toe board, a board from a completed game, 
    and which player the machine player is. 

    The function should score the completed board and update the scores grid. 

    As the function updates the scores grid directly, it does not return anything,
    """

    square_score_funcs = {
        MR_DRAW: _get_square_score_draw,
        MR_REFERENCE_WON: _get_square_score_reference_won,
        MR_OTHER_WON: _get_square_score_other_won
    }

    square_score_func = square_score_funcs[_find_match_result(board, player)]

    for row in xrange(board.get_dim()):
        for col in xrange(board.get_dim()):
            square = board.square(row, col)
            scores[row][col] += square_score_func(square, board, player)


def get_best_move(board, scores):
    """
    This function takes a current board and a grid of scores. 

    The function should find all of the empty squares with the maximum score 
    and randomly return one of them as a (row, column) tuple. 
    """

    empty_squares = board.get_empty_squares()

    max_empty_score = max([scores[idx[0]][idx[1]] for idx in empty_squares])

    best_moves = list()

    for row in xrange(board.get_dim()):
        for col in xrange(board.get_dim()):
            if scores[row][col] == max_empty_score and (row, col) in empty_squares:
                best_moves.append((row, col))

    return random.choice(best_moves)


def mc_move(board, player, trials):
    """
    This function takes a current board, which player the machine player is, 
    and the number of trials to run. 

    The function should use a Monte Carlo simulation to return 
    a move for the machine player in the form of a (row, column) tuple.
    """

    scores = _build_empty_grid(board.get_dim())

    for dummy in xrange(trials):
        board_clone = board.clone()
        mc_trial(board_clone, player)
        mc_update_scores(scores, board_clone, player)

    return get_best_move(board, scores)


def _build_empty_grid(dim):
    """
    Builds an empty grid.
    """

    empty_grid = [
        [float(0) for dummy_col in xrange(dim)]
        for dummy_row in xrange(dim)
    ]

    return empty_grid



# Test game with the console or the GUI.
# Uncomment whichever you prefer.
# Both should be commented out when you submit for
# testing to save time.

# provided.play_game(mc_move, NTRIALS, False)
# poc_ttt_gui.run_gui(3, provided.PLAYERX, mc_move, NTRIALS, False)
