"""
Clone of 2048 game.
"""

import random

# Directions, DO NOT MODIFY

UP = 1
DOWN = 2
LEFT = 3
RIGHT = 4

# Offsets for computing tile indices in each direction.
# DO NOT MODIFY this dictionary.

OFFSETS = {
    UP: (1, 0),
    DOWN: (-1, 0),
    LEFT: (0, 1),
    RIGHT: (0, -1)
}


def merge(line):
    """
    Helper function that merges a single row or column in 2048
    """

    nonzero_line = [element for element in line if element != 0]

    if len(nonzero_line) == 1:
        return nonzero_line + [0] * (len(line) - len(nonzero_line))

    aggr_line, skip = list(), False

    for idx in xrange(len(nonzero_line) - 1):

        if skip:
            skip = False
            continue

        if nonzero_line[idx] == nonzero_line[idx + 1]:
            skip = True
            aggr_line.append(nonzero_line[idx] + nonzero_line[idx + 1])
        else:
            aggr_line.append(nonzero_line[idx])

        # Add the last nonzero value to the aggregated line when:
        # * This is the last nonzero pair and is not equal
        # * The algorithm is going to skip and miss the last nonzero value

        last_pair_not_equal = not skip and (idx == len(nonzero_line) - 2)
        going_to_miss_last = skip and (idx + 2 == len(nonzero_line) - 1)

        if last_pair_not_equal or going_to_miss_last:
            aggr_line.append(nonzero_line[-1])

    return_line = aggr_line + [0] * (len(line) - len(aggr_line))

    return return_line


class TwentyFortyEight(object):

    """
    Class to run the game logic.
    """

    def __init__(self, grid_height, grid_width):
        """
        Constructor
        """

        self._grid_height = grid_height
        self._grid_width = grid_width
        self._initial_tiles = None
        self._grid = None

        self._set_initial_tiles()
        self.reset()

    def _set_initial_tiles(self):
        """
        Sets the dictionary of initial tiles
        """

        self._initial_tiles = {
            UP: [
                (0, 0 + idx)
                for idx in xrange(self.get_grid_width())
            ],
            DOWN: [
                (self.get_grid_height() - 1, 0 + idx)
                for idx in xrange(self.get_grid_width())
            ],
            LEFT: [
                (0 + idx, 0)
                for idx in xrange(self.get_grid_height())
            ],
            RIGHT: [
                (0 + idx, self.get_grid_width() - 1)
                for idx in xrange(self.get_grid_height())
            ]
        }

    def _get_empty_grid(self):
        """
        Builds an empty grid
        """

        empty_grid = [
            [0 for dummy_col in xrange(self._grid_width)]
            for dummy_row in xrange(self._grid_height)
        ]

        return empty_grid

    def reset(self):
        """
        Reset the game so the grid is empty.
        """

        self._grid = self._get_empty_grid()

    def __str__(self):
        """
        Return a string representation of the grid for debugging.
        """

        return "%s instance: %s" % (self.__class__, self._grid)

    def get_grid_height(self):
        """
        Get the height of the board.
        """

        return self._grid_height

    def get_grid_width(self):
        """
        Get the width of the board.
        """

        return self._grid_width

    def move(self, direction):
        """
        Move all tiles in the given direction and add
        a new tile if any tiles moved.
        """

        initial_tiles = self._initial_tiles[direction]

        lines_indexes, merged_values = list(), list()

        limits = {
            UP: self.get_grid_height(),
            DOWN: self.get_grid_height(),
            LEFT: self.get_grid_width(),
            RIGHT: self.get_grid_width()
        }

        for initial_tile in initial_tiles:
            lines_indexes.append([(
                initial_tile[0] + OFFSETS[direction][0] * counter,
                initial_tile[1] + OFFSETS[direction][1] * counter
            ) for counter in xrange(limits[direction])])

        for line in lines_indexes:
            merged_values.append(merge(
                [self.get_tile(idx[0], idx[1]) for idx in line]
            ))

        zipped_indexes_values = [
            zip(lines_indexes[idx], merged_values[idx])
            for idx in xrange(len(lines_indexes))
        ]

        new_grid = self._get_empty_grid()

        for zipped in zipped_indexes_values:
            for idx, value in zipped:
                new_grid[idx[0]][idx[1]] = value

        if new_grid != self.get_grid():
            self._grid = new_grid
            self.new_tile()

    def new_tile(self):
        """
        Create a new tile in a randomly selected empty square. 
        The tile should be 2 90% of the time and 4 10% of the time.
        """

        idx_choices = list()

        for row in xrange(self._grid_height):
            for col in xrange(self._grid_width):
                if self._grid[row][col] == 0:
                    idx_choices.append((row, col))

        if not len(idx_choices):
            return False

        random_idx = random.choice(idx_choices)
        random_val = random.choice([2] * 9 + [4] * 1)

        self.set_tile(random_idx[0], random_idx[1], random_val)

    def set_tile(self, row, col, value):
        """
        Set the tile at position row, col to have the given value.
        """

        self._grid[row][col] = value

    def get_tile(self, row, col):
        """
        Return the value of the tile at position row, col.
        """

        return self._grid[row][col]

    def get_grid(self):
        """
        Return the current grid
        """

        return self._grid

# import poc_2048_gui
# poc_2048_gui.run_gui(TwentyFortyEight(4, 4))
