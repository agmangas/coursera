"""
TwentyFortyEight tests.
"""

import os
import sys

cwd = os.path.dirname(os.path.abspath(sys.modules[__name__].__file__))
sys.path.append(os.path.join(cwd, "..", "..", "modules"))

import poc_simpletest

from twenty_forty_eight import merge, TwentyFortyEight

if __name__ == "__main__":

    suite = poc_simpletest.TestSuite()

    # merge() tests

    suite.run_test(merge([2, 0, 2, 4]), [4, 4, 0, 0], "merge() Test #1")
    suite.run_test(merge([0, 0, 2, 2]), [4, 0, 0, 0], "merge() Test #2")
    suite.run_test(merge([2, 2, 0, 0]), [4, 0, 0, 0], "merge() Test #3")
    suite.run_test(merge([2, 2, 2, 2]), [4, 4, 0, 0], "merge() Test #4")
    suite.run_test(merge([8, 16, 16, 8]), [8, 32, 8, 0], "merge() Test #5")
    suite.run_test(merge([8, 16, 16, 16, 8, 32]), [8, 32, 16, 8, 32, 0], "merge() Test #6")
    suite.run_test(merge([8, 16, 16, 16, 8]), [8, 32, 16, 8, 0], "merge() Test #7")
    suite.run_test(merge([8, 16, 16, 64, 64]), [8, 32, 128, 0, 0], "merge() Test #8")
    suite.run_test(merge([8, 0, 16, 64, 64]), [8, 16, 128, 0, 0], "merge() Test #9")
    suite.run_test(merge([8, 8, 8, 8, 8]), [16, 16, 8, 0, 0], "merge() Test #10")
    suite.run_test(merge([2]), [2], "merge() Test #11")
    suite.run_test(merge([2, 0, 0, 0]), [2, 0, 0, 0], "merge() Test #12")

    # new_tile() tests

    twfe_instance = TwentyFortyEight(4, 5)
    twfe_instance.new_tile()
    grid_values = [val for row in twfe_instance.get_grid() for val in row]
    nonzero_grid_values = [val for val in grid_values if val != 0]

    suite.run_test(len(nonzero_grid_values), 1, "new_tile() Test #1")
    suite.run_test(nonzero_grid_values[0] in (2, 4), True, "new_tile() Test #2")

    suite.report_results()
