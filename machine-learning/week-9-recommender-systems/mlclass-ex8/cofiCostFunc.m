function [J, grad] = cofiCostFunc(params, Y, R, num_users, num_movies, ...
                                  num_features, lambda)
%COFICOSTFUNC Collaborative filtering cost function
%   [J, grad] = COFICOSTFUNC(params, Y, R, num_users, num_movies, ...
%   num_features, lambda) returns the cost and gradient for the
%   collaborative filtering problem.
%

% Unfold the U and W matrices from params
X = reshape(params(1:num_movies*num_features), num_movies, num_features);
Theta = reshape(params(num_movies*num_features+1:end), ...
                num_users, num_features);

            
% You need to return the following values correctly
J = 0;
X_grad = zeros(size(X));
Theta_grad = zeros(size(Theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost function and gradient for collaborative
%               filtering. Concretely, you should first implement the cost
%               function (without regularization) and make sure it is
%               matches our costs. After that, you should implement the 
%               gradient and use the checkCostFunction routine to check
%               that the gradient is correct. Finally, you should implement
%               regularization.
%
% Notes: X - num_movies  x num_features matrix of movie features
%        Theta - num_users  x num_features matrix of user features
%        Y - num_movies x num_users matrix of user ratings of movies
%        R - num_movies x num_users matrix, where R(i, j) = 1 if the 
%            i-th movie was rated by the j-th user
%
% You should set the following variables correctly:
%
%        X_grad - num_movies x num_features matrix, containing the 
%                 partial derivatives w.r.t. to each element of X
%        Theta_grad - num_users x num_features matrix, containing the 
%                     partial derivatives w.r.t. to each element of Theta
%

error = (X * Theta') - Y;
J = (1/2) * sum(sum((error .^ 2) .* R));
reg = (lambda/2) * (sum(Theta(:) .^ 2) + sum(X(:) .^ 2));
J = J + reg;

num_movies = size(R, 1);
num_users = size(R, 2);

for i=1:num_movies
    RTemp = zeros(size(R, 1), size(R, 2));
    RTemp(i, :) = (R(i, :) == 1);
    ThetaTemp = Theta;
    ThetaTemp(find(R(i, :) == 0), :) = 0;
    result = (error .* RTemp) * ThetaTemp;
    X_grad(i, :) = result(i, :);
    XGradReg = lambda * X(i, :);
    X_grad(i, :) = X_grad(i, :) + XGradReg;
end

for j=1:num_users
    RTemp = zeros(size(R, 1), size(R, 2));
    RTemp(:, j) = (R(:, j) == 1);
    XTemp = X;
    XTemp(find(R(:, j) == 0), :) = 0;
    result = XTemp' * (error .* RTemp);
    Theta_grad(j, :) = result(:, j);
    ThetaGradReg = lambda * Theta(j, :);
    Theta_grad(j, :) = Theta_grad(j, :) + ThetaGradReg;
end

% =============================================================

grad = [X_grad(:); Theta_grad(:)];

end
