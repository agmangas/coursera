# -*- coding: utf-8 -*-

"""
Algorithmic Thinking Module 4 Project: 
Computing alignments of sequences
"""

DASH = "-"


def compute_local_alignment(seq_x, seq_y, scoring_matrix, alignment_matrix):
    """Takes as input two sequences seq_x and seq_y whose elements share a common 
    alphabet with the scoring matrix scoring_matrix. This function computes a local 
    alignment of seq_x and seq_y using the local alignment matrix alignment_matrix.

    The function returns a tuple of the form (score, align_x, align_y) where score 
    is the score of the optimal local alignment align_x and align_y. Note that align_x 
    and align_y should have the same length and may include the padding character '-'."""

    # Just for convenience

    amat = alignment_matrix
    smat = scoring_matrix

    # Search for the maximum in the aligment matrix and set the initial (i, j) to its position

    max_each_row = [max(row) for row in amat]
    idx_row_max = max_each_row.index(max(max_each_row))
    idx_row_col = amat[idx_row_max].index(max(amat[idx_row_max]))

    idxi, idxj = idx_row_max, idx_row_col
    align_x, align_y = str(), str()

    while idxi != 0 and idxj != 0 and amat[idxi][idxj] != 0:
        if amat[idxi][idxj] == amat[idxi - 1][idxj - 1] + smat[seq_x[idxi - 1]][seq_y[idxj - 1]]:
            align_x = seq_x[idxi - 1] + align_x
            align_y = seq_y[idxj - 1] + align_y
            idxi -= 1
            idxj -= 1
        else:
            if amat[idxi][idxj] == amat[idxi - 1][idxj] + smat[seq_x[idxi - 1]][DASH]:
                align_x = seq_x[idxi - 1] + align_x
                align_y = DASH + align_y
                idxi -= 1
            else:
                align_x = DASH + align_x
                align_y = seq_y[idxj - 1] + align_y
                idxj -= 1

    while idxi != 0 and amat[idxi][idxj] != 0:
        align_x = seq_x[idxi - 1] + align_x
        align_y = DASH + align_y
        idxi -= 1

    while idxj != 0 and amat[idxi][idxj] != 0:
        align_x = DASH + align_x
        align_y = seq_y[idxj - 1] + align_y
        idxj -= 1

    return amat[idx_row_max][idx_row_col], align_x, align_y


def compute_global_alignment(seq_x, seq_y, scoring_matrix, alignment_matrix):
    """Takes as input two sequences seq_x and seq_y whose elements share a common 
    alphabet with the scoring matrix scoring_matrix. This function computes a global 
    alignment of seq_x and seq_y using the global alignment matrix alignment_matrix.

    The function returns a tuple of the form (score, align_x, align_y) where score is 
    the score of the global alignment align_x and align_y. Note that align_x and align_y 
    should have the same length and may include the padding character '-'."""

    # Just for convenience

    amat = alignment_matrix
    smat = scoring_matrix

    # Set the initial (i, j) to the lower right corner

    idxi, idxj = len(seq_x), len(seq_y)
    align_x, align_y = str(), str()

    while idxi != 0 and idxj != 0:
        if amat[idxi][idxj] == amat[idxi - 1][idxj - 1] + smat[seq_x[idxi - 1]][seq_y[idxj - 1]]:
            align_x = seq_x[idxi - 1] + align_x
            align_y = seq_y[idxj - 1] + align_y
            idxi -= 1
            idxj -= 1
        else:
            if amat[idxi][idxj] == amat[idxi - 1][idxj] + smat[seq_x[idxi - 1]][DASH]:
                align_x = seq_x[idxi - 1] + align_x
                align_y = DASH + align_y
                idxi -= 1
            else:
                align_x = DASH + align_x
                align_y = seq_y[idxj - 1] + align_y
                idxj -= 1

    while idxi != 0:
        align_x = seq_x[idxi - 1] + align_x
        align_y = DASH + align_y
        idxi -= 1

    while idxj != 0:
        align_x = DASH + align_x
        align_y = seq_y[idxj - 1] + align_y
        idxj -= 1

    return amat[-1][-1], align_x, align_y


def _alignment_score(score, global_flag):
    """Returns the appropiate score depending on the global_flag."""

    return int(0) if score < 0 and not global_flag else float(score)


def compute_alignment_matrix(seq_x, seq_y, scoring_matrix, global_flag):
    """Takes as input two sequences seq_x and seq_y whose elements share a common 
    alphabet with the scoring matrix scoring_matrix. The function computes and returns 
    the alignment matrix for seq_x and seq_y as described in the Homework. 
    If global_flag is True, each entry of the alignment matrix is computed using the 
    method described in Question 8 of the Homework. If global_flag is False, each entry 
    is computed using the method described in Question 12 of the Homework."""

    ret = _init_alignment_matrix(len(seq_x) + 1, len(seq_y) + 1)

    len_m, len_n = len(seq_x), len(seq_y)

    ret[0][0] = 0

    for idxi in range(1, len_m + 1):
        ret[idxi][0] = _alignment_score(
            ret[idxi - 1][0] + scoring_matrix[seq_x[idxi - 1]][DASH], global_flag)

    for idxj in range(1, len_n + 1):
        ret[0][idxj] = _alignment_score(
            ret[0][idxj - 1] + scoring_matrix[DASH][seq_y[idxj - 1]], global_flag)

    for idxi in range(1, len_m + 1):
        for idxj in range(1, len_n + 1):
            ret[idxi][idxj] = max([
                _alignment_score(
                    ret[idxi - 1][idxj - 1] + scoring_matrix[seq_x[idxi - 1]][seq_y[idxj - 1]],
                    global_flag),
                _alignment_score(
                    ret[idxi - 1][idxj] + scoring_matrix[seq_x[idxi - 1]][DASH],
                    global_flag),
                _alignment_score(
                    ret[idxi][idxj - 1] + scoring_matrix[DASH][seq_y[idxj - 1]],
                    global_flag)
            ])

    return ret


def build_scoring_matrix(alphabet, diag_score, off_diag_score, dash_score):
    """Takes as input a set of characters alphabet and three scores diag_score, 
    off_diag_score, and dash_score. The function returns a dictionary of dictionaries 
    whose entries are indexed by pairs of characters in alphabet plus '-'. 
    The score for any entry indexed by one or more dashes is dash_score. 
    The score for the remaining diagonal entries is diag_score. 
    Finally, the score for the remaining off-diagonal entries is off_diag_score."""

    ret = dict()

    for letter_key in alphabet:
        ret[letter_key] = dict()
        for letter_iter in alphabet:
            if DASH in (letter_key, letter_iter):
                this_score = dash_score
            elif letter_iter == letter_key:
                this_score = diag_score
            else:
                this_score = off_diag_score
            ret[letter_key][letter_iter] = this_score

    return ret


def _init_alignment_matrix(rows, cols):
    """Returns an empty alignment matrix."""

    ret = list()

    for dummy_row in range(rows):
        ret.append([None] * cols)

    return ret
