# -*- coding: utf-8 -*-

"""
Code related to Module 3 Application questions.
"""

import os
import sys
import timeit
import random
import gc

import matplotlib.pyplot as plt

import alg_project3_viz as viz

CWD = os.path.dirname(os.path.abspath(sys.modules[__name__].__file__))
sys.path.append(os.path.join(CWD, "..", "project"))

import clustering
from alg_cluster import Cluster


def _timeit_without_gc(func, *args, **kwargs):
    """Measure elapsed time without garbage collection. Returns the elapsed time."""

    gc.disable()
    start_time = timeit.default_timer()
    func(*args, **kwargs)
    elapsed = timeit.default_timer() - start_time
    gc.enable()

    return elapsed


def gen_random_clusters(num_clusters):
    """Creates a list of clusters where each cluster in this list corresponds
    to one randomly generated point in the square with corners (±1,±1)."""

    random_clusters = list()

    for dummy_idx in range(num_clusters):
        random_horiz_pos = (random.random() * 2) - 1
        random_vert_pos = (random.random() * 2) - 1
        random_clusters.append(Cluster(set([]), random_horiz_pos, random_vert_pos, 0, 0))

    return random_clusters


def question_10(kmeans_iter=5):
    """Compute the distortion of the list of clusters produced by hierarchical clustering 
    and k-means clustering (using 5 iterations) on the 111, 290, and 896 county data sets, 
    respectively, where the number of output clusters ranges from 6 to 20 (inclusive)."""

    print("*************")
    print("** Question 10")
    print("*************")

    def _singleton_list(data_table):

        singleton_list = []

        for line in data_table:
            singleton_list.append(
                Cluster(set([line[0]]), line[1], line[2], line[3], line[4]))

        return singleton_list

    print(">> Loading datasets")

    data_tables = {
        "111": viz.load_data_table(viz.DATA_111_URL),
        "290": viz.load_data_table(viz.DATA_290_URL),
        "896": viz.load_data_table(viz.DATA_896_URL)
    }

    singleton_lists = {
        "111": _singleton_list(data_tables["111"]),
        "290": _singleton_list(data_tables["290"]),
        "896": _singleton_list(data_tables["896"])
    }

    out_clusters_range = range(6, 21)

    distortions_hierarchical = {
        "111": list(),
        "290": list(),
        "896": list()
    }

    distortions_kmeans = {
        "111": list(),
        "290": list(),
        "896": list()
    }

    for num in out_clusters_range:

        for key in ["111", "290", "896"]:

            print(">> Calculating KMeans for n = {0} and dataset {1}".format(num, key))

            distortions_kmeans[key].append(viz.compute_distortion(
                clustering.kmeans_clustering(singleton_lists[key][:], num, kmeans_iter), data_tables[key]))

            print(">> Calculating Hierarchical for n = {0} and dataset {1}".format(num, key))

            distortions_hierarchical[key].append(viz.compute_distortion(
                clustering.hierarchical_clustering(singleton_lists[key][:], num), data_tables[key]))

    print(">> Plotting")

    for key in ["111", "290", "896"]:

        plt.figure()
        plt.plot(out_clusters_range, distortions_kmeans[key], label="KMeans")
        plt.plot(out_clusters_range, distortions_hierarchical[key], label="Hierarchical")
        plt.legend(loc="lower right")
        plt.grid(True)
        plt.xlabel("Number of output clusters")
        plt.ylabel("Distortion")
        plt.title("KMeans vs Hierarchical distortion (dataset {0} counties)".format(key))


def question_1():
    """Write a function gen_random_clusters(num_clusters) that creates a list of
    clusters where each cluster in this list corresponds to one randomly generated
    point in the square with corners (±1,±1). Use this function and your favorite
    Python timing code to compute the running times of the functions slow_closest_pairs
    and fast_closest_pair for lists of clusters of size 2 to 200.

    Once you have computed the running times for both functions, plot the result as
    two curves combined in a single plot. (Use a line plot for each curve.)
    The horizontal axis for your plot should be the the number of initial clusters
    while the vertical axis should be the running time of the function in seconds.
    Please include a legend in your plot that distinguishes the two curves."""

    print("*************")
    print("** Question 1")
    print("*************")

    num_clusters_range = range(2, 201)
    slow_times, fast_times = list(), list()

    print(">> Measuring execution times")

    for num_clusters in num_clusters_range:
        random_clusters = gen_random_clusters(num_clusters)
        slow_times.append(_timeit_without_gc(clustering.slow_closest_pairs, random_clusters))
        fast_times.append(_timeit_without_gc(clustering.fast_closest_pair, random_clusters))

    plt.figure()
    plt.plot(num_clusters_range, slow_times, label="slow_closest_pairs")
    plt.plot(num_clusters_range, fast_times, label="fast_closest_pair")
    plt.legend(loc="upper left")
    plt.grid(True)
    plt.xlabel("Number of initial clusters")
    plt.ylabel("Time (seconds)")
    plt.title("slow_closest_pairs vs fast_closest_pair (Desktop Python)")


if __name__ == "__main__":

    question_1()
    question_10()
    plt.show()
