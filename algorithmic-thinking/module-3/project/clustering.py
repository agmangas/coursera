# -*- coding: utf-8 -*-

"""
Algorithmic Thinking Module 3 Project: 
Closest pairs and clustering algorithms
"""

import math

import alg_cluster


def pair_distance(cluster_list, idx1, idx2):
    """
    Helper function to compute Euclidean distance between two clusters
    in cluster_list with indices idx1 and idx2
    
    Returns tuple (dist, idx1, idx2) with idx1 < idx2 where dist is distance between
    cluster_list[idx1] and cluster_list[idx2]
    """

    return (cluster_list[idx1].distance(cluster_list[idx2]), min(idx1, idx2), max(idx1, idx2))


def slow_closest_pairs(cluster_list):
    """
    Compute the set of closest pairs of cluster in list of clusters
    using O(n^2) all pairs algorithm
    
    Returns the set of all tuples of the form (dist, idx1, idx2) 
    where the cluster_list[idx1] and cluster_list[idx2] have minimum distance dist.
    """

    ret, min_dist = set(), float("inf")

    for idx_u, cluster_u in enumerate(cluster_list):
        for idx_v, cluster_v in enumerate(cluster_list):

            if idx_u == idx_v:
                continue

            dist_uv = cluster_u.distance(cluster_v)

            if dist_uv < min_dist:
                min_dist = dist_uv
                ret.clear()
                ret.add(pair_distance(cluster_list, idx_u, idx_v))
            elif dist_uv == min_dist:
                ret.add(pair_distance(cluster_list, idx_u, idx_v))

    return ret


def fast_closest_pair(cluster_list):
    """
    Compute a closest pair of clusters in cluster_list
    using O(n log(n)) divide and conquer algorithm
    
    Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
    cluster_list[idx1] and cluster_list[idx2]
    have the smallest distance dist of any pair of clusters
    """

    min_tup_dist = lambda x: x[0]

    def fast_helper(cluster_list, horiz_order, vert_order):
        """
        Divide and conquer method for computing distance between closest pair of points
        Running time is O(n * log(n))
        
        horiz_order and vert_order are lists of indices for clusters
        ordered horizontally and vertically
        
        Returns a tuple (distance, idx1, idx2) with idx1 < idx 2 where
        cluster_list[idx1] and cluster_list[idx2]
        have the smallest distance dist of any pair of clusters
        """

        # Base case

        num_points = len(horiz_order)

        if num_points <= 3:
            bf_cluster_list = [cluster_list[idx] for idx in horiz_order]
            bf_result = slow_closest_pairs(bf_cluster_list).pop()
            return (bf_result[0], horiz_order[bf_result[1]], horiz_order[bf_result[2]])

        # Divide

        num_points_half = int(math.ceil(num_points / 2.0))

        mid_horiz = 0.5 * (
            cluster_list[horiz_order[num_points_half]].horiz_center() +
            cluster_list[horiz_order[num_points_half - 1]].horiz_center()
        )

        horiz_order_lft = horiz_order[:num_points_half]
        horiz_order_rgt = horiz_order[num_points_half:]

        vert_order_lft = list()
        vert_order_rgt = list()

        horiz_order_lft_set = set(horiz_order_lft)

        for vert_order_member in vert_order:
            if vert_order_member in horiz_order_lft_set:
                vert_order_lft.append(vert_order_member)
            else:
                vert_order_rgt.append(vert_order_member)

        ret = min([
            fast_helper(cluster_list, horiz_order_lft, vert_order_lft),
            fast_helper(cluster_list, horiz_order_rgt, vert_order_rgt)
        ], key=min_tup_dist)

        # Conquer

        s_list = list()

        for vert_order_member in vert_order:
            if abs(cluster_list[vert_order_member].horiz_center() - mid_horiz) < ret[0]:
                s_list.append(vert_order_member)

        for idx_u in range(0, len(s_list) - 1):
            for idx_v in range(idx_u + 1, min([idx_u + 4, len(s_list)])):
                ret = min([
                    ret,
                    pair_distance(cluster_list, s_list[idx_u], s_list[idx_v])
                ], key=min_tup_dist)

        return ret

    # Compute list of indices for the clusters ordered in the horizontal direction

    hcoord_and_index = [
        (cluster_list[idx].horiz_center(), idx)
        for idx in range(len(cluster_list))
    ]

    hcoord_and_index.sort()
    horiz_order = [hcoord_and_index[idx][1] for idx in range(len(hcoord_and_index))]

    # Compute list of indices for the clusters ordered in vertical direction

    vcoord_and_index = [
        (cluster_list[idx].vert_center(), idx)
        for idx in range(len(cluster_list))
    ]

    vcoord_and_index.sort()
    vert_order = [vcoord_and_index[idx][1] for idx in range(len(vcoord_and_index))]

    # Compute answer recursively

    answer = fast_helper(cluster_list, horiz_order, vert_order)

    return (answer[0], min(answer[1:]), max(answer[1:]))


def kmeans_clustering(cluster_list, num_clusters, num_iterations):
    """
    Compute the k-means clustering of a set of clusters
    
    Input: List of clusters, number of clusters, number of iterations
    Output: List of clusters whose length is num_clusters
    """

    # Initialize k-means clusters to be initial clusters with largest populations

    ret = sorted(cluster_list, key=lambda cl: cl.total_population(), reverse=True)[:num_clusters]

    # Start doing 'num_iterations' iterations

    for dummy_idx_i in range(num_iterations):

        # Initialize 'num_clusters' empty sets

        iter_clusters = [
            alg_cluster.Cluster(set([]), 0, 0, 0, 0)
            for dummy_idx in range(num_clusters)
        ]

        # Add each point to its nearest cluster

        for idx_j in range(len(cluster_list)):
            distances = [cluster_list[idx_j].distance(ret[idx_f]) for idx_f in range(num_clusters)]
            idx_l = distances.index(min(distances))
            iter_clusters[idx_l].merge_clusters(cluster_list[idx_j])

        # Update centers

        ret = iter_clusters

    return ret


def hierarchical_clustering(cluster_list, num_clusters):
    """
    Compute a hierarchical clustering of a set of clusters
    Note: the function mutates cluster_list
    
    Input: List of clusters, number of clusters
    Output: List of clusters whose length is num_clusters
    """

    while len(cluster_list) > num_clusters:
        (dummy_dist, idx_first, idx_second) = fast_closest_pair(cluster_list)
        cluster_list[idx_first].merge_clusters(cluster_list[idx_second])
        cluster_list.pop(idx_second)

    return cluster_list
