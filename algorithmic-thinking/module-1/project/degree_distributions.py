# -*- coding: utf-8 -*-

"""
Algorithmic Thinking Module 1 Project: 
Degree distributions for graphs
"""

from collections import Counter

# Graph constants

EX_GRAPH0 = {
    0: set([1, 2]),
    1: set([]),
    2: set([])
}

EX_GRAPH1 = {
    0: set([1, 4, 5]),
    1: set([2, 6]),
    2: set([3]),
    3: set([0]),
    4: set([1]),
    5: set([2]),
    6: set([])
}

EX_GRAPH2 = {
    0: set([1, 4, 5]),
    1: set([2, 6]),
    2: set([3, 7]),
    3: set([7]),
    4: set([1]),
    5: set([2]),
    6: set([]),
    7: set([3]),
    8: set([1, 2]),
    9: set([0, 3, 4, 5, 6, 7])
}


def make_complete_graph(num_nodes):
    """Takes the number of nodes num_nodes and returns a dictionary 
    corresponding to a complete directed graph with the specified number of nodes."""

    ret = dict()

    if num_nodes < 1:
        return ret

    for node in range(num_nodes):
        edges = range(num_nodes)
        edges.remove(node)
        ret[node] = set(edges)

    return ret


def compute_in_degrees(digraph):
    """Takes a directed graph digraph (represented as a dictionary) 
    and computes the in-degrees for the nodes in the graph."""

    all_edges = [list(val) for val in digraph.values()]
    all_edges_flat = [edge for sublist in all_edges for edge in sublist]

    counter_results = dict(Counter(all_edges_flat))

    for node in digraph:
        if not node in counter_results:
            counter_results[node] = 0

    return counter_results


def in_degree_distribution(digraph):
    """Takes a directed graph digraph (represented as a dictionary) and computes 
    the unnormalized distribution of the in-degrees of the graph."""

    return dict(Counter(compute_in_degrees(digraph).values()))
