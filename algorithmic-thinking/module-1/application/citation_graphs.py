# -*- coding: utf-8 -*-

"""
Algorithmic Thinking Module 1 Application:
Citation Graphs
"""

# General imports

import os
import sys
import urllib2
import random
import matplotlib.pyplot as plt
import numpy
import dpa

# Import degree_distributions module

CWD = os.path.dirname(os.path.abspath(sys.modules[__name__].__file__))
sys.path.append(os.path.join(CWD, "..", "project"))

from degree_distributions import *

# Constants

CITATION_URL = "http://storage.googleapis.com/codeskulptor-alg/alg_phys-cite.txt"

# Functions


def load_graph(graph_url):
    """
    Function that loads a graph given the URL
    for a text representation of the graph.
    Returns a dictionary that models a graph.
    """

    graph_file = urllib2.urlopen(graph_url)
    graph_text = graph_file.read()
    graph_lines = graph_text.split('\n')
    graph_lines = graph_lines[: -1]

    print(">> Loaded graph with {0} nodes".format(len(graph_lines)))

    answer_graph = {}
    for line in graph_lines:
        neighbors = line.split(' ')
        node = int(neighbors[0])
        answer_graph[node] = set([])
        for neighbor in neighbors[1: -1]:
            answer_graph[node].add(int(neighbor))

    return answer_graph


def er_directed(n, p):
    """ER algorithm modified to generate directed graphs."""

    graph = dict()

    for idxi in range(n):
        graph[idxi] = set()
        for idxj in range(n):
            if random.random() < p and idxj != idxi:
                graph[idxi].add(idxj)

    return graph


def norm_degree_distribution(distr):
    """Returns normalized in/out-degree distribution."""

    ret = dict()

    norm = float(sum(distr.values()))
    for order in distr:
        ret[order] = distr[order] / norm

    return ret


def compute_out_degrees(digraph):
    """Takes a directed graph digraph (represented as a dictionary) 
    and computes the out-degrees for the nodes in the graph."""

    out_degrees = dict()

    for node in digraph:
        out_degrees[node] = len(digraph[node])

    return out_degrees


def out_degree_distribution(digraph):
    """Takes a directed graph digraph (represented as a dictionary) and computes 
    the unnormalized distribution of the out-degrees of the graph."""

    return dict(Counter(compute_out_degrees(digraph).values()))


def question_one(base=10):
    """Your task for this question is to compute the in-degree distribution 
    for this citation graph. Once you have computed this distribution, you should 
    normalize the distribution (make the values in the dictionary sum to one) 
    and then compute a log/log plot of the points in this normalized distribution."""

    print(">> Executing question one")

    citation_graph = load_graph(CITATION_URL)
    indeg_distr = in_degree_distribution(citation_graph)

    # Normalize in degree distribution

    distr_norm = norm_degree_distribution(indeg_distr)

    # Plot log/log

    plt.figure()
    plt.loglog(distr_norm.keys(), distr_norm.values(), ".", basex=base, basey=base)
    plt.grid(True)
    plt.xlabel("log(degree)")
    plt.ylabel("log(prob_degree)")
    plt.title('Citation graph in-degree distribution log/log (base {0})'.format(base))


def question_two(base=10):
    """For this question, your task is to consider the shape of the in-degree distribution 
    for an ER graph and compare its shape to that of the physics citation graph."""

    print(">> Executing question two")

    pairs = [
        (600, 0.2), (600, 0.5), (600, 1.0),
        (3000, 0.2), (3000, 0.5), (3000, 1.0),
        (5000, 0.2), (5000, 0.5), (5000, 0.99)
    ]

    title = 'ER directed(n={0}, p={1}) in-degree distribution log/log (base {2})'

    for n, p in pairs:
        distr = in_degree_distribution(er_directed(n, p))
        distr_norm = norm_degree_distribution(distr)
        plt.figure()
        plt.loglog(distr_norm.keys(), distr_norm.values(), ".", basex=base, basey=base)
        plt.grid(True)
        plt.xlabel("log(degree)")
        plt.ylabel("log(prob_degree)")
        plt.title(title.format(n, p, base))


def question_three(base=10):
    """For this question, we will choose values for n and m that yield a DPA graph 
    whose number of nodes and edges is roughly the same to those of the citation graph."""

    print(">> Executing question three")

    # Generate some random ER graphs and plot in and out
    # distributions to check that both are similar

    pairs = [(3000, 0.2), (3000, 0.5)]

    title = 'ER directed(n={0}, p={1}) in/out-degree distribution log/log (base {2})'

    for n, p in pairs:
        digraph = er_directed(n, p)
        distr_in = in_degree_distribution(digraph)
        distr_out = out_degree_distribution(digraph)
        distr_in_norm = norm_degree_distribution(distr_in)
        distr_out_norm = norm_degree_distribution(distr_out)
        plt.figure()
        plt.loglog(distr_in_norm.keys(), distr_in_norm.values(), ".b", basex=base, basey=base)
        plt.loglog(distr_out_norm.keys(), distr_out_norm.values(), "*r", basex=base, basey=base)
        plt.grid(True)
        plt.legend(["In-degree", "Out-degree"])
        plt.xlabel("log(degree)")
        plt.ylabel("log(prob_degree)")
        plt.title(title.format(n, p, base))

    # Plot in-degree and out-degree distribution for citation graph

    citation_graph = load_graph(CITATION_URL)
    cit_distr_in = in_degree_distribution(citation_graph)
    cit_distr_out = out_degree_distribution(citation_graph)
    cit_distr_in_norm = norm_degree_distribution(cit_distr_in)
    cit_distr_out_norm = norm_degree_distribution(cit_distr_out)

    plt.figure()
    plt.loglog(cit_distr_in_norm.keys(), cit_distr_in_norm.values(), ".b", basex=base, basey=base)
    plt.loglog(cit_distr_out_norm.keys(), cit_distr_out_norm.values(), "*r", basex=base, basey=base)
    plt.grid(True)
    plt.legend(["In-degree", "Out-degree"])
    plt.xlabel("log(degree)")
    plt.ylabel("log(prob_degree)")
    plt.title("Citation graph in/out-degree distribution log/log (base={0})".format(base))

    # Print average out degree

    cit_in_degrees = compute_in_degrees(citation_graph)
    cit_out_degrees = compute_out_degrees(citation_graph)

    assert cit_in_degrees != cit_out_degrees

    print(">> Number of nodes in cit. graph: {0}".format(len(citation_graph)))
    print(">> Avg. cit. graph in degree: {0}".format(numpy.mean(cit_in_degrees.values())))
    print(">> Avg. cit. graph out degree: {0}".format(numpy.mean(cit_out_degrees.values())))


def question_four(n, m, base=10):
    """Your task for this question is to implement the DPA algorithm, compute a DPA graph using 
    the values from Question 3, and then plot the in-degree distribution for this DPA graph."""

    print(">> Executing question four")

    # Plot DPA graph in-degree distribution

    dpa_graph = dpa.dpa(n, m)
    dpa_graph_in = in_degree_distribution(dpa_graph)
    dpa_graph_in_norm = norm_degree_distribution(dpa_graph_in)

    plt.figure()
    plt.loglog(dpa_graph_in_norm.keys(), dpa_graph_in_norm.values(), ".", basex=base, basey=base)
    plt.grid(True)
    plt.xlabel("log(degree)")
    plt.ylabel("log(prob_degree)")
    plt.title("DPA({0}, {1}) in-degree distribution log/log (base {2})".format(n, m, base))

    # Print DPA - citation comparison metrics

    citation_graph = load_graph(CITATION_URL)

    print(">> Cit. graph num. nodes: {0}".format(len(citation_graph)))
    print(">> Cit. graph num. edges: {0}".format(sum(map(len, citation_graph.values()))))
    print(">> DPA graph num. nodes: {0}".format(len(dpa_graph)))
    print(">> DPA graph num. edges: {0}".format(sum(map(len, dpa_graph.values()))))


def question_five(n, m, base=10):
    """In this last problem, we will compare the in-degree distribution for the citation graph 
    to the in-degree distribution for the DPA graph as constructed in Question 4. 
    In particular, we will consider whether the shape of these two distributions 
    are similar and, if they are similar, what might be the cause of the similarity."""

    # Compare DPA and citation graph in-degree distribution log/log plots

    dpa_graph = dpa.dpa(n, m)
    dpa_graph_in = in_degree_distribution(dpa_graph)
    dpa_graph_in_norm = norm_degree_distribution(dpa_graph_in)

    cit_graph = load_graph(CITATION_URL)
    cit_graph_in = in_degree_distribution(cit_graph)
    cit_graph_in_norm = norm_degree_distribution(cit_graph_in)

    plt.figure()
    plt.loglog(dpa_graph_in_norm.keys(), dpa_graph_in_norm.values(), ".b", basex=base, basey=base)
    plt.loglog(cit_graph_in_norm.keys(), cit_graph_in_norm.values(), "*r", basex=base, basey=base)
    plt.grid(True)
    plt.legend(["DPA", "Cit. graph"])
    plt.xlabel("log(degree)")
    plt.ylabel("log(prob_degree)")
    plt.title("Citation graph vs DPA in-degree distr. log/log (base={0})".format(base))

# Entry point

if __name__ == "__main__":

    QUESTION_THREE_N = 27770
    QUESTION_THREE_M = 13

    # question_one()
    # question_two()
    # question_three()
    # question_four(QUESTION_THREE_N, QUESTION_THREE_M)
    # question_five(QUESTION_THREE_N, QUESTION_THREE_M)

    plt.show()
