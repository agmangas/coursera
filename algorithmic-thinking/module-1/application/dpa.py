# -*- coding: utf-8 -*-

"""
Provided code for application portion of module 1

Helper class for implementing efficient version
of DPA algorithm
"""

import os
import sys
import random

# Import degree_distributions module

CWD = os.path.dirname(os.path.abspath(sys.modules[__name__].__file__))
sys.path.append(os.path.join(CWD, "..", "project"))

from degree_distributions import *


class DPATrial:

    """
    Simple class to encapsulate optimized trials for DPA algorithm
    
    Maintains a list of node numbers with multiple instances of each number.
    The number of instances of each node number are
    in the same proportion as the desired probabilities
    
    Uses random.choice() to select a node number from this list for each trial.
    """

    def __init__(self, num_nodes):
        """
        Initialize a DPATrial object corresponding to a 
        complete graph with num_nodes nodes
        
        Note the initial list of node numbers has num_nodes copies of
        each node number
        """

        self._num_nodes = num_nodes
        self._node_numbers = [node for node in range(num_nodes) for dummy_idx in range(num_nodes)]

    def run_trial(self, num_nodes):
        """
        Conduct num_node trials using by applying random.choice()
        to the list of node numbers
        
        Updates the list of node numbers so that the number of instances of
        each node number is in the same ratio as the desired probabilities
        
        Returns:
        Set of nodes
        """

        # Compute the neighbors for the newly-created node

        new_node_neighbors = set()
        for dummy_idx in range(num_nodes):
            new_node_neighbors.add(random.choice(self._node_numbers))

        # Update the list of node numbers so that each node number
        # appears in the correct ratio

        self._node_numbers.append(self._num_nodes)
        self._node_numbers.extend(list(new_node_neighbors))

        # Update the number of nodes

        self._num_nodes += 1

        return new_node_neighbors


def dpa(n, m):
    """Returns directed graph generated using DPA algorithm."""

    graph = make_complete_graph(m)

    dpa_trial = DPATrial(m)

    for idx in range(m, n):
        new_nodes = dpa_trial.run_trial(m)
        graph[idx] = set()
        for new_node in new_nodes:
            graph[idx].add(new_node)

    return graph
