# -*- coding: utf-8 -*-

"""
Code to generate three type of undirected graphs:
* Fixed example computer network
* ER graphs
* UPA graphs
"""

import random
import itertools

import provided


def example_computer_network_graph():
    """Returns the example computer network graph loaded from the Internet."""

    return provided.load_graph(provided.NETWORK_URL)


def er_graph(n, p):
    """Implementation of the ER algorithm to generated random undirected graphs."""

    graph = dict()

    for node in range(n):
        graph[node] = set()

    for pair in itertools.combinations(graph.keys(), 2):
        if random.random() < p:
            graph[pair[0]].add(pair[1])
            graph[pair[1]].add(pair[0])

    return graph


def make_complete_graph(num_nodes):
    """Takes the number of nodes num_nodes and returns a dictionary 
    corresponding to a complete graph with the specified number of nodes."""

    ret = dict()

    if num_nodes < 1:
        return ret

    for node in range(num_nodes):
        edges = range(num_nodes)
        edges.remove(node)
        ret[node] = set(edges)

    return ret


def upa_graph(n, m):
    """Returns undirected graph generated using UPA algorithm."""

    graph = make_complete_graph(m)

    upa_trial = provided.UPATrial(m)

    for idx in range(m, n):
        new_node_neighbors = upa_trial.run_trial(m)
        graph[idx] = set()
        for new_node_neighbor in new_node_neighbors:
            graph[idx].add(new_node_neighbor)
            graph[new_node_neighbor].add(idx)

    return graph
