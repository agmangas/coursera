# -*- coding: utf-8 -*-

"""
Code related to Module 2 Application questions.
"""

import os
import sys
import random
import timeit
import gc

import matplotlib.pyplot as plt

CWD = os.path.dirname(os.path.abspath(sys.modules[__name__].__file__))
sys.path.append(os.path.join(CWD, "..", "project"))

import ccs_resilience
import example_graphs
import provided


def er_params_from_ugraph(ugraph):
    """Find parameters for ER(n, p) so the resulting ER graph has the 
    same number of nodes and approximately the same number of edges."""

    this_ugraph_len = ugraph_len(ugraph)
    selected_n = this_ugraph_len[0]
    max_number_edges = (this_ugraph_len[0] * (this_ugraph_len[0] - 1)) / 2.0
    selected_p = this_ugraph_len[1] / max_number_edges

    return int(selected_n), selected_p


def upa_params_from_ugraph(ugraph):
    """Find parameters for UPA(n, m) so the resulting UPA graph has the 
    same number of nodes and approximately the same number of edges."""

    this_ugraph_len = ugraph_len(ugraph)
    selected_n = this_ugraph_len[0]
    avg_degree = float(this_ugraph_len[1]) / this_ugraph_len[0]
    selected_m = round(avg_degree)

    return int(selected_n), int(selected_m)


def ugraph_len(ugraph):
    """Returns the number of nodes and edges in an ugraph as a tuple (n, m)."""

    n = len(ugraph)
    m = sum([len(nodes_set) for nodes_set in ugraph.values()]) / 2

    return n, m


def random_order(ugraph):
    """Takes a graph and returns a list of the nodes in the graph in some random order."""

    list_nodes = ugraph.keys()
    random.shuffle(list_nodes)

    return list_nodes


def fast_targeted_order(ugraph):
    """Efficient implementation of provided.targeted_order."""

    # Copy graph

    copied_graph = provided.copy_graph(ugraph)

    # Get size of graph in O(1)

    n = len(copied_graph)

    # Create DegreeSets in O(n)

    degree_sets = [set() for k in range(n)]

    for node in copied_graph:
        node_order = len(copied_graph[node])
        degree_sets[node_order].add(node)

    # Initializations in O(1)

    l_ret = list()

    # The first for / while loops over every node: O(n)

    for k in reversed(range(n)):
        while len(degree_sets[k]):

            # Statements in O(1)

            u = degree_sets[k].pop()
            u_neighs = copied_graph[u]

            # This for loop will result in O(m) == O(5n) == O(n) for
            # all the iterations of the main loop combined

            for v in u_neighs:
                d = len(copied_graph[v])
                degree_sets[d].remove(v)
                degree_sets[d - 1].add(v)

            # Statements in O(1)

            l_ret.append(u)

            # Same reasoning as the for loop above: O(n) all iterations combined

            copied_graph.pop(u)
            for v in u_neighs:
                copied_graph[v].remove(u)

    # Return result in O(1)

    return l_ret


def question_1():
    """To begin our analysis, we will examine the resilience of the computer
    network under an attack in which servers are chosen at random.
    We will then compare the resilience of the network to the
    resilience of ER and UPA graphs of similar size."""

    print("*************")
    print("** Question 1")
    print("*************")

    comp_network_graph = example_graphs.example_computer_network_graph()

    er_params = er_params_from_ugraph(comp_network_graph)
    sample_er_graph = example_graphs.er_graph(er_params[0], er_params[1])

    upa_params = upa_params_from_ugraph(comp_network_graph)
    sample_upa_graph = example_graphs.upa_graph(upa_params[0], upa_params[1])

    print(">> Computer network graph (n, m) = {0}".format(ugraph_len(comp_network_graph)))
    print(">> Sample ER graph (n, p) = {0}".format(ugraph_len(sample_er_graph)))
    print(">> Sample UPA graph (n, m) = {0}".format(ugraph_len(sample_upa_graph)))

    graphs_data = [
        {
            "graph": comp_network_graph,
            "title": "Computer network",
            "plotargs": "b"
        },
        {
            "graph": sample_er_graph,
            "title": "ER graph (p={0})".format(round(er_params[1], 5)),
            "plotargs": "y"
        },
        {
            "graph": sample_upa_graph,
            "title": "UPA graph (m={0})".format(upa_params[1]),
            "plotargs": "g"
        }
    ]

    plt.figure()

    for graph_data in graphs_data:
        print(">> Attacking graph: {0}".format(graph_data["title"]))
        attack_order = random_order(graph_data["graph"])
        resilience = ccs_resilience.compute_resilience(graph_data["graph"], attack_order)
        plt.plot(resilience, graph_data["plotargs"], label=graph_data["title"])
        plt.legend()

    plt.grid(True)
    plt.xlabel("Number of nodes removed")
    plt.ylabel("Size of the largest connected component")
    plt.title("Comparison of graphs resilience")


def _timeit_without_gc(func, *args, **kwargs):
    """Measure elapsed time without garbage collection. Returns the elapsed time."""

    gc.disable()
    start_time = timeit.default_timer()
    func(*args, **kwargs)
    elapsed = timeit.default_timer() - start_time
    gc.enable()

    return elapsed


def question_3(m=5):
    """For this question, your task is to implement fast_targeted_order and then analyze 
    the running time of these two methods on UPA graphs of size n with m=5. 
    Your analysis should be both mathematical and empirical."""

    print("*************")
    print("** Question 3")
    print("*************")

    n_range = range(10, 1000, 10)
    fast_times, orig_times = list(), list()

    print(">> Measuring execution times")

    for n in n_range:
        sample_upa_graph = example_graphs.upa_graph(n, m)
        orig_times.append(_timeit_without_gc(provided.targeted_order, sample_upa_graph))
        fast_times.append(_timeit_without_gc(fast_targeted_order, sample_upa_graph))

    plt.figure()
    plt.plot(n_range, orig_times, label="targeted_order")
    plt.plot(n_range, fast_times, label="fast_targeted_order")
    plt.legend(loc="upper left")
    plt.grid(True)
    plt.xlabel("Size of graph (n)")
    plt.ylabel("Time (seconds)")
    plt.title("Running times of targeted order algorithms (Desktop Python 2.7.2)")

    # Plot loglog to verify Big-O

    plt.figure()
    plt.loglog(n_range, map(lambda x: x * 1e-6, n_range), label="f(x)=x", linewidth=4)
    plt.loglog(n_range, orig_times, 'k', label="targeted_order")
    plt.loglog(n_range, map(lambda x: (x ** 2) * 1e-6, n_range), label="f(x)=x^2", linewidth=4)
    plt.loglog(n_range, fast_times, 'y', label="fast_targeted_order")
    plt.legend()
    plt.grid(True)
    plt.xlabel("Log of size of graph (n)")
    plt.ylabel("Log of time (seconds)")
    plt.title("Loglog of running times of targeted order algorithms")


def question_4():
    """Using targeted_order (or fast_targeted_order), your task is to compute a targeted attack 
    order for each of the three graphs (computer network, ER, UPA) from Question 1. 
    Then, for each of these three graphs, compute the resilience of the graph using compute_resilience. 
    Finally, plot the computed resiliences as three curves (line plots) in a single plot."""

    print("*************")
    print("** Question 4")
    print("*************")

    comp_network_graph = example_graphs.example_computer_network_graph()

    er_params = er_params_from_ugraph(comp_network_graph)
    sample_er_graph = example_graphs.er_graph(er_params[0], er_params[1])

    upa_params = upa_params_from_ugraph(comp_network_graph)
    sample_upa_graph = example_graphs.upa_graph(upa_params[0], upa_params[1])

    print(">> Computer network graph (n, m) = {0}".format(ugraph_len(comp_network_graph)))
    print(">> Sample ER graph (n, p) = {0}".format(ugraph_len(sample_er_graph)))
    print(">> Sample UPA graph (n, m) = {0}".format(ugraph_len(sample_upa_graph)))

    graphs_data = [
        {
            "graph": comp_network_graph,
            "title": "Computer network",
            "plotargs": "b"
        },
        {
            "graph": sample_er_graph,
            "title": "ER graph (p={0})".format(round(er_params[1], 5)),
            "plotargs": "y"
        },
        {
            "graph": sample_upa_graph,
            "title": "UPA graph (m={0})".format(upa_params[1]),
            "plotargs": "g"
        }
    ]

    plt.figure()

    for graph_data in graphs_data:
        print(">> Attacking graph with fast_targeted_order: {0}".format(graph_data["title"]))
        attack_order = fast_targeted_order(graph_data["graph"])
        resilience = ccs_resilience.compute_resilience(graph_data["graph"], attack_order)
        plt.plot(resilience, graph_data["plotargs"], label=graph_data["title"])
        plt.legend()

    plt.grid(True)
    plt.xlabel("Number of nodes removed")
    plt.ylabel("Size of the largest connected component")
    plt.title("Comparison of graphs resilience using fast_targeted_order attack")


if __name__ == "__main__":

    question_1()
    question_3()
    question_4()
    plt.show()
