# -*- coding: utf-8 -*-

"""
Algorithmic Thinking Module 2 Project: 
Connected components and graph resilience
"""

from collections import deque


def bfs_visited(ugraph, start_node):
    """Takes the undirected graph ugraph and the node start_node and 
    returns the set consisting of all nodes that are visited by a 
    breadth-first search that starts at start_node."""

    queue, visited = deque(), set()

    visited.add(start_node)
    queue.append(start_node)

    while len(queue):
        popped = queue.popleft()
        for neigh in ugraph[popped]:
            if not neigh in visited:
                visited.add(neigh)
                queue.append(neigh)

    return visited


def cc_visited(ugraph):
    """Takes the undirected graph ugraph and returns a list of sets, 
    where each set consists of all the nodes (and nothing else) in a 
    connected component, and there is exactly one set in the list for 
    each connected component in ugraph and nothing else."""

    remaining_nodes = set(ugraph.keys())
    conn_comps = list()

    while len(remaining_nodes):
        popped = remaining_nodes.pop()
        visited_nodes = bfs_visited(ugraph, popped)
        conn_comps.append(visited_nodes)
        remaining_nodes -= visited_nodes

    return conn_comps


def largest_cc_size(ugraph):
    """"Takes the undirected graph ugraph and returns the size 
    (an integer) of the largest connected component in ugraph."""

    cc_sizes = [len(conn_comp) for conn_comp in cc_visited(ugraph)]
    return max(cc_sizes) if len(cc_sizes) else int(0)


def compute_resilience(ugraph, attack_order):
    """Takes the undirected graph ugraph, a list of nodes attack_order and 
    iterates through the nodes in attack_order. For each node in the list, 
    the function removes the given node and its edges from the graph and then 
    computes the size of the largest connected component for the resulting graph.

    The function should return a list whose k+1th entry is the size of the largest 
    connected component in the graph after the removal of the first k nodes in 
    attack_order. The first entry (indexed by zero) is the size of the largest 
    connected component in the original graph."""

    cc_size_evolution = list()
    cc_size_evolution.append(largest_cc_size(ugraph))

    mutated_ugraph = dict(ugraph)

    for attacked_node in attack_order:
        del mutated_ugraph[attacked_node]
        for node in mutated_ugraph:
            mutated_ugraph[node] -= set([attacked_node])
        cc_size_evolution.append(largest_cc_size(mutated_ugraph))

    return cc_size_evolution
